# Copyright (c) Alvarium Project Maintainers
# Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
# All Rights Reserved. Use is subject to license terms.
#
# The usage of this project makes mandatory the authors citation in
# any scientific publication or technical reports. For websites or
# research projects the Alvarium website and logo needs to be linked
# in a visible area. Please check the project website for more details.
#
# All the files of the Alvarium Project are subject of this license,
#  unless stated otherwise. All the libraries, sounds and graphic elements
#  used in the project are subject to their own license.
#
#  Alvarium is free software: you can redistribute them and/or modify
#  them under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env bash

alvariumLibFolder="lib"

getDependencies() {
    returnDependencies=$(find ${libraryPath} -type f | tr '\n' ':')
    returnDependencies=${returnDependencies::-1}
}

setupProductionPath() {
    # Production path setup
    prodPath='./'
    if [ ! -e "${prodPath}${alvariumLibFolder}" ]; then
        if [ -e "../${prodPath}${alvariumLibFolder}" ]; then
            # the alternative production path exists
            prodPath="../${prodPath}"
        else
            echo "The production path does not exist: ${prodPath}. Please run ant release first ..."
            exit 1
        fi
    fi
}

setupLibraryPath() {
    # Libraries setup
    libraryPath="${prodPath}lib/"
    if [ ! -e ${libraryPath} ]; then
        echo "The library path does not exist: ${libraryPath}"
        exit 1
    fi
}

setupResPath() {
    # Resources path setup
    resPath="${prodPath}res/"
    if [ ! -e ${resPath} ]; then
        echo "The res path does not exist: ${resPath}"
        exit 1
    fi
}

initPaths() {
    setupProductionPath
    setupLibraryPath
    setupResPath
}