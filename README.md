**Disclaimer: ** This is currently a work in progress and the development process
is relatively slow. Please contact me directly if you have any questions.

# Key concepts of Alvarium

Alvarium is the second version of MyBlock, the base of AgentSlang Project
(http://agent.roboslang.org). This enables developers to build rich, distributed 
and fast Interactive Systems. Alvarium ensures the component-to-component communication, 
dealing with data transmission in an efficient way. It adds a transparent layer 
of communication so that AgentSlang components do not have to deal with all 
these concepts. The main improvement from the MyBlock version is that Alvarium
added a series of Container Managers, which mediates the communication process.  

The choice of a certain Message Queue protocol implementation has been done on 
licence availability, popularity and performance. In particular, we benchmarked 
ZeroMQ over ActiveMQ, to confirm that ZeroMQ (our choice) is a faster and reliable 
candidate, which supports multiple connection types, different communication 
patterns, the possibility to send binary data and the absence of a broker 
component, which slows down a distributed pipeline architecture.

## Data Oriented design

Even if the formats do not have to be strictly identical between linked 
components, the compatibility has to be ensured at least. There are two main 
directions in this area:

1. Design data to have a small transfer size and memory footprint
2. Generic data representation, written in standard formats (i.e. JSON or XML)

Google Protocol Buffers presents a comparison between their own serialization 
format and the XML parsing and conclude that their format is 10 to 100 times 
faster than XML. MsgPack follows the same direction of small memory footprint 
data representation, by offering better performance than Google Protocol Buffers, 
while maintaining the multi-language binding and multi-platform support.

We therefore propose to define our own Object-Oriented data representation. 
Thanks to object hierarchies, objects are extensible and independent from the 
data serialization level. This allows us to change the serialization level in 
the future if needed. Currently, the serialization is done with MsgPack due to 
the best performance. The data has a small memory footprint than in the case of 
XML or JSON, has very fast serialization mechanisms and offers the advantages of 
working with native Object Structures rather than XML trees or JSON maps.

## Alvarium Components

A component is an atomic structure for the Alvarium platform. The component 
processes a given set of data types and forward the output to the next component 
in the chain. The internal flow of a component can have two different aspects: 
either it is a reactive output to the input, or an active component that can 
produce output based on the internal states without any input. Special components 
are elements which only consume (Sink) or produce (Source), without any mixed function.

At this level, data types are an important aspect. The data exchanged need to be 
compatible between linked elements. A component formally defines preconditions 
and post-conditions in terms of data types, in order to be linked with other 
components to form complex processing pipes. The communication protocol between 
two components is a simple publish-subscribe architecture to ease data exchange.

## Alvarium Services

Similarly to the component, we define the services. A service is designed is to 
respond to requests that can be triggered by any component or other service. 
The communication protocol used for services is a synchronous request-reply.

In the current implementation, the services exist only as part of the container 
and the container protocol manages the communication.

# Wiki

Please check the [wiki](https://gitlab.com/agentslang/alvarium/wikis/home) for the install and development tutorial

# More details

Please check the AgentSlang webpage: http://agent.roboslang.org/

# Logo credits

Honey jar icon provided by http://icons8.com/