/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.utils;

import org.alvarium.component.base.Publisher;
import org.alvarium.logger.Logger;

import java.io.File;

public class FileUtils {
    public static File createCacheDir(Publisher concernedComponent, String cacheDir) {
        return createCacheDir(concernedComponent, cacheDir == null ? null : new File(cacheDir));
    }

    public static File createCacheDir(Publisher concernedComponent, File cacheDir) {
        if (cacheDir == null) {
            cacheDir = new File("tmpCache");
        }
        File resultCache = cacheDir.getAbsoluteFile();

        if (!resultCache.exists()) {
            if (!resultCache.mkdir()) {
                Logger.log(concernedComponent, Logger.Level.CRITICAL, "Cannot create audio cache in path = " +
                                                                      resultCache.getAbsolutePath());
                throw new IllegalArgumentException("Invalid cache path provided: " + resultCache.getAbsolutePath());
            }
        } else if (!resultCache.canWrite()) {
            Logger.log(concernedComponent, Logger.Level.CRITICAL, "Cannot write in given path = " + resultCache.getAbsolutePath());
            throw new IllegalArgumentException("Invalid cache path provided: " + resultCache.getAbsolutePath());
        }
        Logger.log(concernedComponent, Logger.Level.INFORM, "Using cache: " + resultCache);
        return resultCache;
    }

    public static boolean checkReadableFile(File filePath, boolean failOnInvalid) {
        if (filePath.exists() && filePath.canRead()) {
            return true;
        } else {
            if (failOnInvalid) {
                throw new IllegalArgumentException("Cannot read the given path = " + filePath.getAbsolutePath());
            } else {
                return false;
            }
        }
    }

    public static boolean checkWritableFile(File filePath, boolean failOnInvalid) {
        if (filePath.exists() && filePath.canWrite()) {
            return true;
        } else {
            if (failOnInvalid) {
                throw new IllegalArgumentException("Cannot write on given path = " + filePath.getAbsolutePath());
            } else {
                return false;
            }
        }
    }

    public static boolean checkExecutableFile(File filePath, boolean failOnInvalid) {
        if (filePath.exists() && filePath.canExecute()) {
            return true;
        } else {
            if (failOnInvalid) {
                throw new IllegalArgumentException("Cannot find executable on given path = " + filePath.getAbsolutePath());
            } else {
                return false;
            }
        }
    }
}
