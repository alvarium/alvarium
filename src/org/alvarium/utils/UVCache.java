/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.utils;

import java.util.HashMap;
import java.util.Map;

public class UVCache<U, V> {
    private final Map<U, V> ucache = new HashMap<>();
    private final Map<V, U> vcache = new HashMap<>();

    public UVCache() {
    }

    public synchronized void put(U u, V v) {
        if (!ucache.containsKey(u) && !vcache.containsKey(v)) {
            ucache.put(u, v);
            vcache.put(v, u);
        }
    }

    public synchronized boolean containsU(U u) {
        return ucache.containsKey(u);
    }

    public synchronized boolean containesV(V v) {
        return vcache.containsKey(v);
    }

    public synchronized V getV(U u) {
        return ucache.get(u);
    }

    public synchronized U getU(V v) {
        return vcache.get(v);
    }

    public synchronized V removeU(U u) {
        V v = ucache.remove(u);
        if (v != null) {
            vcache.remove(v);
        }
        return v;
    }

    public synchronized U removeV(V v) {
        U u = vcache.get(v);
        if (u != null) {
            ucache.remove(u);
        }
        return u;
    }

    public synchronized void removeAll() {
        ucache.clear();
        vcache.clear();
    }
}
