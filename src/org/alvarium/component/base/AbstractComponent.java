/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.base;

import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.communication.PublishOutboundManager;
import org.alvarium.component.communication.SubscribeInboundManager;
import org.alvarium.component.consistency.ConsistencyManager;
import org.alvarium.component.consistency.DataChecker;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SystemHeartbeat;
import org.alvarium.component.service.Scheduler;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;

import java.util.HashMap;
import java.util.Map;

import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.container.monitor.SystemMonitorManager.systemMonitorManager;
import static org.alvarium.logger.Logger.Level.*;
import static org.alvarium.logger.Logger.log;

public abstract class AbstractComponent implements ActiveListener, Publisher, Closeable, ReactiveListener {
    protected PublishOutboundManager publishOutboundManager;
    protected SubscribeInboundManager subscribeInboundManager;

    protected ComponentConfig config;

    private final Map<String, String> topicMapper = new HashMap<>();

    protected static final String INBOUND_GROUP = "__INBOUND_";

    private ConsistencyManager consistencyManager;

    public AbstractComponent(ComponentConfig config) {
        this.config = config;

        consistencyManager = new ConsistencyManager();

        publishOutboundManager = new PublishOutboundManager(this, config.getAddress());
        subscribeInboundManager = new SubscribeInboundManager(this);
        addTypeCheckers();
    }

    public final void react(GenericData data) {
        log(this, DEBUG, "Component %s received data=%s", getComponentId(), data);
        if (data instanceof SystemHeartbeat) {
            if (this.act()) {
                // beat --
                systemMonitorManager.fireAct(getComponentId());
            }
        } else {
            if (checkConsistency(INBOUND_GROUP, data)) {
                log(this, DEBUG, "Component %s handles data=%s", getComponentId(), data);
                handleData(data);
                // beat --
                systemMonitorManager.fireAct(getComponentId());
            }
        }
    }

    public void setupComponentConfig(ListProperties config) {
        setupComponent(config);
        systemMonitorManager.fireComponentStarted(getComponentId());
    }

    protected abstract void setupComponent(ListProperties config);

    protected abstract void handleData(GenericData data);

    private boolean checkConsistency(String group, GenericData data) {
        return consistencyManager.check(group, data, true);
    }

    private void addTypeChecker(String group, Class<? extends GenericData> type) {
        consistencyManager.addChecker(group, new DataChecker(type));
    }

    private void addInboundTypeChecker(Class<? extends GenericData> type) {
        addTypeChecker(INBOUND_GROUP, type);
    }

    public void addOutboundTypeChecker(String internalTopic, Class<? extends GenericData> type) {
        addTypeChecker(internalTopic, type);
    }

    @SuppressWarnings("unchecked")
    private void addTypeCheckers() {
        ConfigureParams configureParams = getClass().getAnnotation(ConfigureParams.class);
        if (configureParams != null) {
            if (configureParams.inputDataTypes() != null) {
                for (Class clazz : configureParams.inputDataTypes()) {
                    if (GenericData.class.isAssignableFrom(clazz)) {
                        addInboundTypeChecker(clazz);
                    }
                }
            }

            if (configureParams.outputChannels() != null && configureParams.outputDataTypes() != null &&
                configureParams.outputChannels().length == configureParams.outputDataTypes().length) {
                for (int i = 0; i < configureParams.outputChannels().length; i++) {
                    if (configureParams.outputDataTypes()[i] != org.alvarium.component.data.Void.class &&
                        GenericData.class.isAssignableFrom(configureParams.outputDataTypes()[i])) {
                        addOutboundTypeChecker(configureParams.outputChannels()[i], configureParams
                                                                                            .outputDataTypes()[i]);
                    }
                }
            }
        } else {
            log(this, CRITICAL, getClass().getSimpleName() + " is not properly annotated");
        }
    }

    public void subscribeToScheduler(ComponentId schedulerId) {
        subscribeInboundManager.subscribe(Scheduler.heartbeatTopic, this, schedulerId);
        addInboundTypeChecker(SystemHeartbeat.class);
    }

    public void unsubscribeFromScheduler(ComponentId schedulerId) {
        subscribeInboundManager.unsubscribe(Scheduler.heartbeatTopic, schedulerId);
    }

    public void publish(String externalTopic, String internalTopic) {
        if (!topicMapper.containsKey(internalTopic)) {
            mapInternalTopic(externalTopic, internalTopic);
            componentManager.addTopic(externalTopic, config.getComponentId());
        } else {
            log(this, INFORM, "Duplicate entry external topic=%s, internal topic = %s", externalTopic, internalTopic);
        }
    }

    public void unpublish(String externalTopic, String internalTopic) {
        unmapInternalTopic(internalTopic);
        componentManager.removeTopic(externalTopic, config.getComponentId());
    }

    public void subscribe(String topic, ComponentId remoteComponent, ReactiveListener listener) {
        subscribeInboundManager.subscribe(topic, listener, remoteComponent);
    }

    public void unsubscribe(String topic, ComponentId remoteComponent) {
        subscribeInboundManager.unsubscribe(topic, remoteComponent);
    }

    public void unsubscribe(ComponentId remoteComponent) {
        subscribeInboundManager.unsubscribe(remoteComponent);
    }

    public void publishData(String internalTopic, GenericData data) {
        if (checkConsistency(internalTopic, data)) {
            String externalTopic = getExternalTopic(internalTopic);
            if (externalTopic != null) {
                publishOutboundManager.publishData(externalTopic, data);
            } else {
                // Nobody subscribed to this information ...
                // Did you consider not publishing it ?
                log(this, DEBUG, "Nobody subscribed to channel (%s), but the component(%s) is publishing.",
                    internalTopic, getComponentId());
            }
        }
    }

    @Override
    public ComponentId getComponentId() {
        return config.getComponentId();
    }

    private void mapInternalTopic(String externalTopic, String internalTopic) {
        if (consistencyManager.getDefinedGroups().contains(internalTopic)) {
            synchronized (topicMapper) {
                topicMapper.put(internalTopic, externalTopic);
            }
        } else {
            log(this, CRITICAL, "Invalid internal topic for mapping defined: " + internalTopic);
        }
    }

    private void unmapInternalTopic(String internalTopic) {
        synchronized (topicMapper) {
            topicMapper.remove(internalTopic);
        }
    }

    private String getExternalTopic(String internalTopic) {
        synchronized (topicMapper) {
            return topicMapper.get(internalTopic);
        }
    }

    public void close() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            //-- ignore
        }

        subscribeInboundManager.close();
        publishOutboundManager.close();

        systemMonitorManager.fireComponentClosed(getComponentId());
    }
}
