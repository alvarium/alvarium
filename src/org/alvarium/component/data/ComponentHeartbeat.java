/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.data;

import org.alvarium.container.datatypes.ComponentId;


@TypeIdentification(typeID = 10)
public class ComponentHeartbeat implements Heartbeat {
    private String sourceID;
    private String timestamp;

    public ComponentHeartbeat() {
    }

    public ComponentHeartbeat(String sourceID, long timestamp) {
        this.sourceID = sourceID;
        this.timestamp = "" + timestamp;
    }

    public ComponentHeartbeat(ComponentId sourceID, long timestamp) {
        this.sourceID = sourceID.toString();
        this.timestamp = "" + timestamp;
    }

    public String getId() {
        return timestamp;
    }

    public void setId(String id) {
        this.timestamp = id;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    @Override
    public String toString() {
        return "ComponentHeartbeat{" +
                "sourceID='" + sourceID + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
