/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.data;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.datatypes.TopicKey;
import org.alvarium.logger.Logger;
import org.alvarium.utils.ByteUtils;
import org.msgpack.MessagePack;
import org.reflections.Reflections;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.Set;

import static org.alvarium.container.manager.ComponentManager.componentManager;

public class DataHelper {
    private static final MessagePack packer = new MessagePack();
    private static final DataFactory factory = new DataFactory(packer);

    public static void registerAllPrimitiveClasses() {
        // here register any funny type
        // Note: the Exception class cannot be registered
    }

    public static boolean registerAllGenericDataClasses() {
        registerAllPrimitiveClasses();

        Reflections reflections = new Reflections("");
        Set<Class<? extends GenericData>> subTypes = reflections.getSubTypesOf(GenericData.class);

        Logger.log(DataHelper.class, Logger.Level.DEBUG, "Found subtypes: " + subTypes);

        boolean result = subTypes.size() > 0;
        for (Class<? extends GenericData> item : subTypes) {
            if (!item.isAnonymousClass() && !Modifier.isAbstract(item.getModifiers())) {
                if (item.isAnnotationPresent(TypeIdentification.class)) {
                    try {
                        TypeIdentification annotation = item.getAnnotation(TypeIdentification.class);
                        boolean success = true;

                        for (Class primitive : annotation.primitiveRegisters()) {
                            if (primitive != void.class) {
                                try {
                                    packer.register(primitive);
                                } catch (Throwable e) {
                                    success = false;
                                    result = false;
                                    Logger.log(DataHelper.class, Logger.Level.CRITICAL, "Unable to register primitive" +
                                            " type: " + primitive.getName() + "\n" +
                                            "Check if the class has a " +
                                            "default constructor, a getter and a setter for all the fields.", e);
                                }
                            }
                        }

                        if (success) {
                            factory.registerType(item);
                        }
                    } catch (Exception e) {
                        Logger.log(DataHelper.class, Logger.Level.CRITICAL, "Class not properly annotated for Type Registration: " +
                                item.getName() + "\nCheck if the class has a default constructor, a getter and a setter for all the fields.", e);
                        result = false;
                    }
                } else {
                    Logger.log(DataHelper.class, Logger.Level.CRITICAL, "Class not properly annotated for Type Registration: " + item
                            .getName());
                    result = false;
                }
            }
        }
        if (result) {
            factory.printTypeAssociations();
        }
        return result;
    }

    public static DecodeResult decodeData(byte[] buffer) throws InvalidDataException {
        try {
            return new DecodeResult(componentManager.getTopic(extractTopic(buffer)), factory
                    .readObject(extractData(buffer)));
        } catch (IOException e) {
            throw new InvalidDataException(e);
        }
    }

    public static byte[] encodeData(String topic, ComponentId componentId,
                                    GenericData data) throws InvalidDataException {
        try {
            return ByteUtils.merge(componentManager.getEncoding(topic, componentId), factory.writeObject(data));
        } catch (IOException e) {
            throw new InvalidDataException(e);
        }
    }

    public static GenericData decodeRequestData(byte[] buffer) throws InvalidDataException {
        try {
            return factory.readObject(buffer);
        } catch (IOException e) {
            throw new InvalidDataException(e);
        }
    }

    public static byte[] encodeRequestData(GenericData data) throws InvalidDataException {
        try {
            return factory.writeObject(data);
        } catch (IOException e) {
            throw new InvalidDataException(e);
        }
    }

    private static byte[] extractTopic(byte[] data) {
        return ByteUtils.extract(data, 0, TopicEncodingKey.ENCODING_SIZE);
    }

    private static byte[] extractData(byte[] data) {
        return ByteUtils.extract(data, TopicEncodingKey.ENCODING_SIZE, data.length - TopicEncodingKey.ENCODING_SIZE);
    }

    public static class DecodeResult {
        TopicKey topicKey;
        GenericData data;

        public DecodeResult(TopicKey topicKey, GenericData data) {
            this.topicKey = topicKey;
            this.data = data;
        }

        public TopicKey getTopicKey() {
            return topicKey;
        }

        public GenericData getData() {
            return data;
        }
    }
}
