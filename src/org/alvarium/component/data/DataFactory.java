/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.data;

import org.alvarium.logger.Logger;
import org.alvarium.utils.ByteUtils;
import org.msgpack.MessagePack;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class DataFactory {
    public static final byte[] EMPTY_BUFFER = new byte[0];

    private final Map<Integer, Class> factoryTypes = new HashMap<>();
    private MessagePack packer;

    public DataFactory(MessagePack packer) {
        this.packer = packer;
    }

    public void registerType(Class clazz) {
        synchronized (factoryTypes) {
            TypeIdentification identification = (TypeIdentification) clazz.getAnnotation(TypeIdentification.class);
            if (identification != null) {
                int typeID = identification.typeID();
                Class another = factoryTypes.get(typeID);
                if (another == null || another.equals(clazz)) {
                    factoryTypes.put(typeID, clazz);
                    packer.register(clazz);
                } else {
                    throw new IllegalArgumentException("Invalid typeID provided, Generic Type is already registered: " + another.getName() + " != " + clazz.getName());
                }
            } else {
                throw new IllegalArgumentException("Type class is not annotated with the TypeIdentification: " + clazz.getName());
            }
        }
    }

    public byte[] writeObject(GenericData genericData) throws IOException {
        TypeIdentification identification = genericData.getClass().getAnnotation(TypeIdentification.class);
        if (identification != null) {
            int typeID = identification.typeID();
            Class another = factoryTypes.get(typeID);

            if (another == null || !another.equals(genericData.getClass())) {
                throw new IllegalArgumentException("Invalid typeID provided, Generic Type is already registered: " + genericData.getClass().getName());
            } else {
                byte[] className = packer.write(typeID);
                byte[] dataBuffer = packer.write(genericData);

                return ByteUtils.merge(className, dataBuffer);
            }
        }
        return EMPTY_BUFFER;
    }

    public GenericData readObject(byte[] buffer) throws IOException {
        ByteBuffer bbuffer = ByteBuffer.wrap(buffer);

        int typeID = packer.read(bbuffer, Integer.class);
        Class classType = factoryTypes.get(typeID);
        if (classType == null) {
            throw new IllegalArgumentException("Invalid typeID provided, Generic Type is not registered!");
        } else {
            GenericData result = (GenericData) packer.read(bbuffer, classType);
            if (result == null) {
                throw new IllegalArgumentException("Invalid typeID provided, Generic Type is NULL!");
            } else {
                return result;
            }
        }
    }

    public void printTypeAssociations() {
        StringBuilder sb = new StringBuilder();
        sb.append("Class type associations: \n");
        for (Map.Entry<Integer, Class> item : factoryTypes.entrySet()) {
            sb.append("\t").append(item.getValue().getName()).append(" -> ").append(item.getKey()).append("\n");
        }
        Logger.log(DataFactory.class, Logger.Level.INFORM, sb.deleteCharAt(sb.length() - 1).toString());
    }
}
