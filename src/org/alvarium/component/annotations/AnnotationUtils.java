/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.annotations;

import org.alvarium.component.base.AbstractComponent;
import org.alvarium.logger.Logger;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


public class AnnotationUtils {
    public static Set<String> getAvailableComponents() {
        final Reflections reflections = new Reflections("");
        Set<Class<? extends AbstractComponent>> subTypes = reflections.getSubTypesOf(AbstractComponent.class);

        return subTypes.stream().filter(item -> !item.isAnonymousClass())
                .filter(item -> !Modifier.isAbstract(item.getModifiers()))
                .map(Class::getName).collect(Collectors.toSet());
    }

    public static Set<String> getAvailableProperties(String className) {
        Set<String> result = new HashSet<>();

        try {
            Class classLoaded = Class.forName(className);
            ConfigureParams params = (ConfigureParams) classLoaded.getAnnotation(ConfigureParams.class);

            if (params != null && params.mandatoryConfigurationParams() != null) {
                for (String param : params.mandatoryConfigurationParams()) {
                    if (param.trim().length() > 0) {
                        result.add(param.trim());
                    }
                }
            }

            if (params != null && params.optionalConfigurationParams() != null) {
                for (String param : params.optionalConfigurationParams()) {
                    if (param.trim().length() > 0) {
                        result.add(param.trim());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            //-- ignore
        }

        return result;
    }

    public static Set<String> getAvailableOutputChannels(String className) {
        Set<String> result = new HashSet<>();

        try {
            Class classLoaded = Class.forName(className);
            ConfigureParams params = (ConfigureParams) classLoaded.getAnnotation(ConfigureParams.class);

            if (params != null && params.outputChannels() != null) {
                for (String param : params.outputChannels()) {
                    if (param.trim().length() > 0) {
                        result.add(param.trim());
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            //-- ignore
        }

        return result;
    }

    public static void main(String[] args) {
        for (String item : getAvailableComponents()) {
            StringBuilder sb = new StringBuilder();
            sb.append("Class: ").append(item).append("\n");
            Set<String> results = getAvailableProperties(item);
            if (!results.isEmpty()) {
                sb.append("\tProperties:").append("\n");
                for (String result : results) {
                    sb.append("\t\t").append(result).append("\n");
                }
            }
            results = getAvailableOutputChannels(item);
            if (!results.isEmpty()) {
                sb.append("\tChannels:").append("\n");
                for (String result : results) {
                    sb.append("\t\t").append(result).append("\n");
                }
            }
            sb.append("\n");
            Logger.log(AnnotationUtils.class, Logger.Level.DEBUG, sb.toString());
        }
    }

}
