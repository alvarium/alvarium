/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.communication;

import org.alvarium.component.base.Publisher;
import org.alvarium.component.base.ReactiveListener;
import org.alvarium.component.data.DataHelper;
import org.alvarium.component.data.InvalidDataException;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicKey;
import org.alvarium.logger.Logger;
import org.zeromq.ZMQ;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.alvarium.container.manager.ComponentManager.componentManager;

public class SubscribeInboundManager extends Thread {
    private class TopicWrapper {
        private ReactiveListener reactiveListener;
        private TcpAddress address;
        private String connectionPattern;

        private TopicWrapper(ReactiveListener reactiveListener, ComponentId componentId) {
            this.reactiveListener = reactiveListener;
            this.address = componentManager.resolve(componentId);
            this.connectionPattern = String.format("%s://%s", TcpAddress.protocol, address.toPattern());
        }
    }

    private Map<TopicKey, TopicWrapper> topicMapper = new HashMap<>();

    private boolean running = true;

    private Map<TcpAddress, Integer> connectedHosts = new HashMap<>();

    private ZMQ.Context context;
    private ZMQ.Socket receiver;

    private Publisher component;

    public SubscribeInboundManager(Publisher component) {
        this.component = component;

        context = ZMQ.context(1);
        receiver = context.socket(ZMQ.SUB);

        setName(component.getComponentId() + " Thread");
    }

    public void close() {
        running = false;
    }

    public void run() {
        while (running) {
            try {
                DataHelper.DecodeResult data = DataHelper.decodeData(receiver.recv());
                if (data.getTopicKey() != null) {
                    TopicWrapper topicWrapper = topicMapper.get(data.getTopicKey());
                    if (topicWrapper != null) {
                        topicWrapper.reactiveListener.react(data.getData());
                    } else {
                        Logger.log(component, Logger.Level.INFORM, "(Inbound) Received data for an invalid feed ...");
                    }
                } else {
                    Logger.log(component, Logger.Level.INFORM, "(Inbound) Received data for an invalid feed ...");
                }
            } catch (InvalidDataException e) {
                Logger.log(component, Logger.Level.INFORM, "Invalid data exception: " + e, e);
            } catch (ClassCastException e) {
                Logger.log(component, Logger.Level.INFORM, "Class cast exception: " + e, e);
            }
        }
        //shutdown 
        receiver.close();
        context.term();
    }

    public void subscribe(String topic, ReactiveListener listener, ComponentId componentId) {
        TopicKey topicKey = new TopicKey(topic, componentId);
        if (!topicMapper.containsKey(topicKey)) {
            TopicWrapper topicWrapper = new TopicWrapper(listener, componentId);
            if (!connectedHosts.containsKey(topicWrapper.address)) {
                receiver.connect(topicWrapper.connectionPattern);
                connectedHosts.put(topicWrapper.address, 1);
            } else {
                Integer count = connectedHosts.get(topicWrapper.address);
                connectedHosts.put(topicWrapper.address, count + 1);
            }
            receiver.subscribe(componentManager.getEncoding(topic, componentId));
            topicMapper.put(topicKey, topicWrapper);

            if (!isAlive()) {
                start();
            }
        } else {
            Logger.log(SubscribeInboundManager.class, Logger.Level.INFORM, "Duplicate entry topic key = %s", topicKey);
        }
    }

    public void unsubscribe(String topic, ComponentId componentId) {
        TopicKey topicKey = new TopicKey(topic, componentId);
        unsubscribe(topicKey);
    }

    private void unsubscribe(TopicKey topicKey) {
        TopicWrapper topicWrapper = topicMapper.get(topicKey);
        if (topicWrapper != null) {
            receiver.unsubscribe(componentManager.getEncoding(topicKey.getTopic(), topicKey.getComponentId()));
            Integer count = connectedHosts.get(topicWrapper.address);
            if (count != null && count > 1) {
                connectedHosts.put(topicWrapper.address, count - 1);
            } else {
                connectedHosts.remove(topicWrapper.address);
                receiver.disconnect(topicWrapper.connectionPattern);
            }
            topicMapper.remove(topicKey);
        }
    }

    public void unsubscribe(ComponentId componentId) {
        Set<TopicKey> topicKeys = new HashSet<>(topicMapper.keySet());
        topicKeys.stream().filter(tk -> componentId.equals(tk.getComponentId())).forEach(this::unsubscribe);
    }
}
