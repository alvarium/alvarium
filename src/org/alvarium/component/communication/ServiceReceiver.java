/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.communication;

import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.InvalidDataException;
import org.alvarium.component.data.SystemHeartbeat;
import org.alvarium.container.datatypes.ComponentId;
import org.zeromq.ZMQ;

import static org.alvarium.component.data.DataHelper.decodeRequestData;
import static org.alvarium.component.data.DataHelper.encodeRequestData;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

public class ServiceReceiver extends Thread {
    private ZMQ.Context context;
    private ZMQ.Socket sender;
    private boolean running = true;
    private ComponentId componentId;

    private ServiceHandler handler;

    public ServiceReceiver(ComponentId componentId, int port, int ioThreads, ServiceHandler handler) {
        this.componentId = componentId;
        this.handler = handler;

        context = ZMQ.context(ioThreads);
        sender = context.socket(ZMQ.REP);
        sender.bind("tcp://*:" + port);
    }

    public void run() {
        while (running) {
            byte[] data = sender.recv(0);
            if (data != null) {
                try {
                    GenericData request = convertData(data);
                    GenericData reply;
                    if (request instanceof SystemHeartbeat) {
                        reply = new SystemHeartbeat(System.currentTimeMillis());
                    } else {
                        reply = handleRequest(request);
                    }
                    if (reply != null) {
                        data = convertData(reply);
                        sender.send(data, 0);
                    } else {
                        log(ServiceReceiver.class, CRITICAL, "Invalid data on component = " + componentId);
                    }
                } catch (InvalidDataException e) {
                    log(ServiceReceiver.class, CRITICAL, "Invalid data on component = " + componentId, e);
                }
            }
        }
        sender.close();
        context.term();
    }

    protected GenericData handleRequest(GenericData message) {
        if (handler != null) {
            GenericData result = handler.handle(message);
            log(ServiceReceiver.class, INFORM, "Handle request, request=%s reply=%s", message, result);
            return result;
        } else {
            log(ServiceReceiver.class, CRITICAL, "Invalid handler provided component = " + componentId);
            return null;
        }
    }

    protected byte[] convertData(GenericData data) throws InvalidDataException {
        return encodeRequestData(data);
    }

    protected GenericData convertData(byte[] data) throws InvalidDataException {
        return decodeRequestData(data);
    }

    public void close() {
        running = false;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            //--
        }
    }
}
