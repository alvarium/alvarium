/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.communication;

import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.InvalidDataException;
import org.alvarium.component.data.SystemHeartbeat;
import org.alvarium.component.data.Void;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.logger.Logger;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

import static org.alvarium.component.data.DataHelper.decodeRequestData;
import static org.alvarium.component.data.DataHelper.encodeRequestData;

public class ServiceConnector {
    public static final int TIMEOUT = 1000;

    private ComponentId componentId;
    private String serviceName;
    private long lastPing = 0;
    private TcpAddress address;

    private ZMQ.Context context;
    private ZMQ.Socket sender;

    public ServiceConnector(ComponentId componentId, String serviceName, TcpAddress address) {
        this.address = address;
        this.componentId = componentId;
        this.serviceName = serviceName;

        context = ZMQ.context(1);
        sender = context.socket(ZMQ.REQ);
        sender.setSendTimeOut(TIMEOUT);
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public long getLastPing() {
        return lastPing;
    }

    public void ping() {
        GenericData response = request(new SystemHeartbeat());
        if (!(response instanceof SystemHeartbeat)) {
            lastPing = -1;
        }
    }

    public GenericData request(GenericData message) {
        try {
            if (!sender.send(encodeRequestData(message))) {
                Logger.log(ServiceConnector.class, Logger.Level.CRITICAL, "Failed to send message: " + message);
                return new Void();
            } else {
                lastPing = System.currentTimeMillis();
                return decodeRequestData(sender.recv(0));
            }
        } catch (InvalidDataException | ZMQException e) {
            Logger.log(ServiceConnector.class, Logger.Level.CRITICAL, "Invalid data on service id = " + componentId +
                                                                      " service name = " + serviceName, e);
            return new Void();
        }
    }

    public TcpAddress getAddress() {
        return address;
    }

    public void open() {
        String connectPattern = String.format("%s://%s:%s", TcpAddress.protocol, address.getHost(), address.getPort());
        sender.connect(connectPattern);
    }

    public void close() {
        sender.close();
        context.term();
    }
}
