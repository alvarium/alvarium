/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.consistency;

import org.alvarium.component.data.GenericData;

import java.util.*;

public class ConsistencyManager {
    public static final String ANY = "*";

    private final Map<String, List<ConsistencyChecker>> checkers = new HashMap<String, List<ConsistencyChecker>>();

    public ConsistencyManager() {
    }

    public void addChecker(String group, ConsistencyChecker checker) {
        synchronized (checkers) {
            List<ConsistencyChecker> items = checkers.get(group);
            if (items == null) {
                items = new LinkedList<>();
                checkers.put(group, items);
            }
            items.add(checker);
        }
    }

    public void removeChecker(String group, ConsistencyChecker checker) {
        synchronized (checkers) {
            List<ConsistencyChecker> items = checkers.get(group);
            if (items != null) {
                items.remove(checker);
                if (items.isEmpty()) {
                    checkers.remove(group);
                }
            }
        }
    }

    /*
     * !! Data is invalid by default
     */
    public boolean check(String group, GenericData data, boolean returnOnFirst) {
        synchronized (checkers) {
            if (!ANY.equals(group) && checkers.get(group) == null && checkers.get(ANY) == null) {
                return true;
            }

            boolean valid = false;

            List<ConsistencyChecker> items = checkers.get(group);
            if (items != null) {
                for (ConsistencyChecker checker : items) {
                    valid |= checker.check(data);
                    if (valid && returnOnFirst) {
                        return valid;
                    }
                }
            }

            if (ANY.equals(group)) {
                return valid;
            } else {
                return valid || check(ANY, data, returnOnFirst);
            }
        }
    }

    public Set<String> getDefinedGroups() {
        synchronized (checkers) {
            return Collections.unmodifiableSet(checkers.keySet());
        }
    }
}
