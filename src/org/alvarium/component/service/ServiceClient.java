/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.service;

import org.alvarium.component.base.Closeable;
import org.alvarium.component.communication.ServiceConnector;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.Void;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ServiceClient implements Closeable {
    private static final long SLEEP = 60 * 1000;

    private String serviceName;
    private final List<ServiceConnector> connectors = new LinkedList<>();

    private Thread pingThread;

    public ServiceClient(String serviceName) {
        this.serviceName = serviceName;

        pingThread = new Thread(this::ping);
        pingThread.start();
    }

    public void addConnector(ComponentId componentId, TcpAddress tcpAddress) {
        if (componentId != null && tcpAddress != null) {
            synchronized (connectors) {
                ServiceConnector connector = new ServiceConnector(componentId, serviceName, tcpAddress);
                connectors.add(connector);
                connector.open();
            }
        }
    }

    public void removeConnector(ComponentId componentId) {
        if (componentId != null) {
            synchronized (connectors) {
                Iterator<ServiceConnector> connectorIterator = connectors.iterator();
                while (connectorIterator.hasNext()) {
                    ServiceConnector connector = connectorIterator.next();
                    if (componentId.equals(connector.getComponentId())) {
                        connectorIterator.remove();
                        connector.close();
                    }
                }
            }
        }
    }

    public boolean isAvailable() {
        return !connectors.isEmpty();
    }

    public GenericData request(GenericData inquiry) {
        if (connectors.isEmpty()) {
            throw new IllegalArgumentException("Service not available: " + serviceName);
        } else if (connectors.size() == 1) {
            ServiceConnector connector = connectors.get(0);
            GenericData result = connector.request(inquiry);
            if (result instanceof Void) {
                throw new IllegalArgumentException("Service not available: " + serviceName);
            } else {
                return result;
            }
        } else {
            List<ServiceConnector> available = new LinkedList<>(connectors);
            while (!available.isEmpty()) {
                Optional<ServiceConnector> connector = available.stream().findAny();
                GenericData result = connector.get().request(inquiry);
                if (result instanceof Void) {
                    available.remove(connector.get());
                } else {
                    return result;
                }
                if (connector.isPresent()) {
                    return connector.get().request(inquiry);
                }
            }
            throw new IllegalArgumentException("Service not available: " + serviceName);
        }
    }

    private void ping() {
        boolean running = true;
        while (running) {
            try {
                Thread.sleep(SLEEP);
            } catch (InterruptedException e) {
                running = false;
            }

            if (running) {
                connectors.stream().filter(connector -> System.currentTimeMillis() - connector.getLastPing() > SLEEP)
                          .forEach(ServiceConnector::ping);
            }
        }
    }

    @Override
    public void close() {
        if (pingThread != null) {
            pingThread.interrupt();
        }

        connectors.stream().forEach(ServiceConnector::close);
    }
}
