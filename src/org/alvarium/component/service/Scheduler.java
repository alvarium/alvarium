/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.component.service;

import org.alvarium.component.base.Closeable;
import org.alvarium.component.base.Publisher;
import org.alvarium.component.communication.PublishOutboundManager;
import org.alvarium.component.data.GenericData;
import org.alvarium.component.data.SystemHeartbeat;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.model.SchedulerConfig;

import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

public class Scheduler extends Thread implements Publisher, Closeable {
    private int timeout = 100;
    private boolean running = true;
    protected PublishOutboundManager heartbeatManager;

    public final static String heartbeatTopic = "schedule.heartbeat";

    private ComponentId componentId;

    public Scheduler(SchedulerConfig schedulerConfig) {
        this.componentId = schedulerConfig.getComponentId();
        this.timeout = schedulerConfig.getTimeout();
        heartbeatManager = new PublishOutboundManager(this, schedulerConfig.getAddress());

        componentManager.addTopic(heartbeatTopic, componentId);
    }

    @Override
    public void addOutboundTypeChecker(String internalTopic, Class<? extends GenericData> type) {
        //-- ignore this
    }

    public void kill() {
        running = false;
        interrupt();
    }

    public void run() {
        while (running) {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                running = false;
            }
            if (running) {
                log(Scheduler.class, DEBUG, "Scheduler %s sending hearth beat!", componentId);
                heartbeatManager.publishData(heartbeatTopic, new SystemHeartbeat(System.currentTimeMillis()));
            }
        }
        heartbeatManager.close();
    }

    public void publish(String externalTopic, String internalTopic) {
        componentManager.addTopic(externalTopic, componentId);
    }

    public void publishData(String topic, GenericData data) {
        heartbeatManager.publishData(topic, data);
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public void close() {
        kill();
    }
}
