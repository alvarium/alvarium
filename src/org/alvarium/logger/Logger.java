/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.logger;

import org.apache.logging.log4j.LogManager;
import org.alvarium.component.base.Publisher;

public class Logger {
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(Logger.class);

    public enum Level {
        DEBUG(org.apache.logging.log4j.Level.DEBUG),
        INFORM(org.apache.logging.log4j.Level.INFO),
        CRITICAL(org.apache.logging.log4j.Level.ERROR);

        private org.apache.logging.log4j.Level level;

        Level(org.apache.logging.log4j.Level level) {
            this.level = level;
        }
    }

    public static void log(Class source, Level level, String message, Exception e) {
        logger.log(level.level, "{} - {}", source.getName(), message);
        if (e != null) {
            logger.error(message, e);
        }
    }

    public static void log(Class source, Level level, String pattern, Object... objects) {
        log(source, level, String.format(pattern, objects));
    }

    public static void log(Publisher source, Level level, String pattern, Object... objects) {
        log(source.getClass(), level, pattern, objects);
    }

    public static void log(Class source, Level level, String message) {
        log(source, level, message, (Exception) null);
    }

    public static void log(Publisher source, Level level, String message) {
        log(source, level, message, (Exception) null);
    }

    public static void log(Publisher source, Level level, String message, Exception e) {
        log(source.getClass(), level, message, e);
    }
}
