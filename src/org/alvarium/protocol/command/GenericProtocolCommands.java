/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.command;

import org.alvarium.protocol.parameters.GossipTable;
import org.alvarium.protocol.parameters.ProtocolParameter;

import static org.alvarium.protocol.ProtocolRole.any;

public enum GenericProtocolCommands implements ProtocolCommand {
    // introduce protocol
    // a. buzz + containerId + sourceAddress -> buzz-echo + gossip table (OK) | offensive name (Reject)
    //      i. offensive name -> disconnect
    buzz("buzz", 100),
    buzzEcho("buzz echo", 101, GossipTable.class),
    // offensive name reply
    obuzzName("offensive buzz-name", 102),
    //busy while initialising communication
    buzzy("buzzy", 150),
    // disconnect protocol
    byebye("bye-bye", 200),
    farewell("farewell", 201),
    // ping
    hey("hey", 300),
    ho("ho", 301),
    //placeholder command, can be safely ignored
    broadcastResult("broadcast result", 2),
    // offensive commands, exceptions
    invalid("offensive rtfm", 0),
    obuzz_rtfm("offensive buzz-rtfm", 1);

    private String name;
    private int value;
    private Class<? extends ProtocolParameter> paramClass;

    GenericProtocolCommands(String name, int value) {
        this(name, value, null);
    }

    GenericProtocolCommands(String name, int value, Class<? extends ProtocolParameter> paramClass) {
        this.name = name;
        this.value = value;
        this.paramClass = paramClass;

        ProtocolCommandFactory.register(any, this);
        ProtocolCommandFactory.registerClass(paramClass);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public Class<? extends ProtocolParameter> getParamClass() {
        return paramClass;
    }
}
