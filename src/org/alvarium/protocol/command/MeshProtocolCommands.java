/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.command;

import org.alvarium.protocol.parameters.*;

import static org.alvarium.protocol.ProtocolRole.mesh;

/*
    3. lookup(component)
     */
public enum MeshProtocolCommands implements ProtocolCommand {
    // negotiate header
    //    b. negotiate-request container header -> negotiate-response = {ack, reject}
    //    c. negotiate-result = {success, failed} -> negotiate-result-response + ack (mesh node becomes active now)
    negRequest("negotiate request", 110, HeaderKey.class),
    negRequestAck("negotiate request ack", 111),
    negRequestRej("negotiate request reject", 112, RejectReason.class),
    negResult("negotiate result", 120, TcpAddressParameter.class),
    negResultAck("negotiate result ack", 121),
    loadContainer("load container", 700, ProjectRelatedParameter.class),
    loadContainerAck("load container ack", 701),
    loadContainerRej("load container reject", 702, RejectReason.class),
    rollbackContainer("rollback container", 750),
    rollbackContainerAck("rollback container ack", 751),
    componentManagerChanged("component manager update", 800, ComponentManagerChangeEvent.class),
    componentManagerChangedAck("component manager update ack", 801);

    private String name;
    private int value;
    private Class<? extends ProtocolParameter> paramClass;

    MeshProtocolCommands(String name, int value) {
        this(name, value, null);
    }

    MeshProtocolCommands(String name, int value, Class<? extends ProtocolParameter> paramClass) {
        this.name = name;
        this.value = value;
        this.paramClass = paramClass;

        ProtocolCommandFactory.register(mesh, this);
        ProtocolCommandFactory.registerClass(paramClass);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public Class<? extends ProtocolParameter> getParamClass() {
        return paramClass;
    }
}
