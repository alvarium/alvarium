/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.command;

import org.alvarium.protocol.ProtocolRole;
import org.alvarium.protocol.parameters.ProtocolParameter;

import java.util.HashMap;
import java.util.Map;

import static org.alvarium.protocol.MessageFactory.messageFactory;
import static org.alvarium.protocol.command.GenericProtocolCommands.invalid;

public class ProtocolCommandFactory {
    private static final Map<ProtocolRole, Map<Integer, ProtocolCommand>> valueMap = new HashMap<>();

    public static void register(ProtocolRole role, ProtocolCommand command) {
        synchronized (valueMap) {
            registerCommand(role, command);
            if (role == ProtocolRole.admin || role == ProtocolRole.mesh) {
                registerCommand(ProtocolRole.any, command);
            } else if (role == ProtocolRole.any) {
                registerCommand(ProtocolRole.admin, command);
                registerCommand(ProtocolRole.mesh, command);
            }
        }
    }

    private static void registerCommand(ProtocolRole role, ProtocolCommand command) {
        Map<Integer, ProtocolCommand> protocolMap = valueMap.get(role);
        if (protocolMap == null) {
            protocolMap = new HashMap<>();
            valueMap.put(role, protocolMap);
        }
        ProtocolCommand existingCommand = protocolMap.get(command.getValue());
        if (existingCommand == null || existingCommand == command) {
            protocolMap.put(command.getValue(), command);
        } else {
            throw new IllegalArgumentException("Unable to register protocol command: " + command + ". " +
                                               "Existing command: " + existingCommand);
        }
    }

    public static void registerClass(Class<? extends ProtocolParameter> paramClass) {
        messageFactory.register(paramClass);
    }

    public static ProtocolCommand create(ProtocolRole role, int commandValue) {
        synchronized (valueMap) {
            Map<Integer, ProtocolCommand> protocolMap = valueMap.get(role);
            if (protocolMap == null) {
                return invalid;
            }
            ProtocolCommand command = protocolMap.get(commandValue);
            return command == null ? invalid : command;
        }
    }
}
