/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol;

import org.alvarium.component.data.InvalidDataException;
import org.alvarium.protocol.command.ProtocolCommand;
import org.alvarium.protocol.command.ProtocolCommandFactory;
import org.alvarium.protocol.parameters.ClassDependencies;
import org.alvarium.protocol.parameters.ProtocolParameter;
import org.alvarium.utils.ByteUtils;
import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MessageFactory {
    private static final MessagePack packer = new MessagePack();

    public static final MessageFactory messageFactory = new MessageFactory();

    public void register(Class clazz) {
        if (clazz != null) {
            try {
                if (clazz.isAnnotationPresent(ClassDependencies.class)) {
                    ClassDependencies annotation = (ClassDependencies) clazz.getAnnotation(ClassDependencies.class);
                    for (Class dependency : annotation.primitiveRegisters()) {
                        if (dependency != void.class) {
                            packer.register(dependency);
                        }
                    }
                }
                packer.register(clazz);
            } catch (MessageTypeException e) {
                throw new IllegalArgumentException("Could not register class = " + clazz.getCanonicalName(), e);
            }
        }
    }

    public byte[] writeObject(ProtocolMessage protocolMessage) throws InvalidDataException {
        try {
            byte[] protocolData = packer.write(protocolMessage.getProtocolCommand().getValue());
            protocolData = ByteUtils.merge(protocolData, packer.write(protocolMessage.getSource()));
            protocolData = ByteUtils.merge(protocolData, packer.write(protocolMessage.getTarget()));
            protocolData = ByteUtils.merge(protocolData, packer.write(protocolMessage.getSourceRole().getValue()));
            if (protocolMessage.getProtocolParameter() != null) {
                protocolData = ByteUtils.merge(protocolData, packer.write(protocolMessage.getProtocolParameter()));
            }
            return protocolData;
        } catch (IOException e) {
            throw new InvalidDataException(e);
        }
    }

    public ProtocolParameter readObject(ProtocolCommand command, ByteBuffer buffer) throws InvalidDataException {
        if (buffer.hasRemaining()) {
            Class classType = command.getParamClass();
            if (classType == null) {
                throw new IllegalArgumentException("Invalid typeID provided, Protocol Parameter is not registered. " +
                                                           "Command: " + command.getName());
            } else {
                try {
                    ProtocolParameter result = (ProtocolParameter) packer.read(buffer, classType);
                    if (result == null) {
                        throw new IllegalArgumentException("Invalid typeID provided, Generic Type is NULL!");
                    } else {
                        return result;
                    }
                } catch (IOException e) {
                    throw new InvalidDataException(e);
                }
            }
        } else {
            return null;
        }
    }

    public ProtocolMessage readObject(byte[] buffer) throws InvalidDataException {
        if (buffer != null && buffer.length > 0) {
            ByteBuffer bbuffer = ByteBuffer.wrap(buffer);
            try {
                int commandValue = packer.read(bbuffer, Integer.class);
                String source = packer.read(bbuffer, String.class);
                String target = packer.read(bbuffer, String.class);
                Byte roleValue = packer.read(bbuffer, Byte.class);
                ProtocolRole role = ProtocolRole.fromValue(roleValue);
                ProtocolCommand command = ProtocolCommandFactory.create(role, commandValue);
                ProtocolParameter parameters = readObject(command, bbuffer);
                return new ProtocolMessage(command, source, target, role, parameters);
            } catch (IOException e) {
                throw new InvalidDataException(e);
            }
        } else {
            throw new InvalidDataException("Null reply message");
        }
    }
}
