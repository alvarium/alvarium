/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.processor;

import org.alvarium.component.base.Closeable;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.ProtocolRole;
import org.alvarium.protocol.RemoteConnector;
import org.alvarium.protocol.command.ProtocolCommand;
import org.alvarium.protocol.parameters.ProtocolParameter;

import java.util.HashSet;
import java.util.Set;

import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.command.GenericProtocolCommands.hey;
import static org.alvarium.protocol.command.GenericProtocolCommands.ho;

public class RequestProcessor implements Closeable {
    private static final long SLEEP = 60 * 1000;

    private RemoteConnector connector;

    private Set<ProtocolCommand> busyReplies = new HashSet<>();
    private String source;
    private ProtocolRole role;

    private int maxRepeatsOnBusy = 10;

    private Thread pingThread;

    public RequestProcessor(String source, ProtocolRole role, RemoteConnector connector) {
        this.source = source;
        this.role = role;
        this.connector = connector;

        pingThread = new Thread(this::ping);
        pingThread.start();
    }

    public RequestProcessor addBusyReply(ProtocolCommand command) {
        busyReplies.add(command);
        return this;
    }

    public RequestProcessor setMaxRepeatsOnBusy(int maxRepeatsOnBusy) {
        this.maxRepeatsOnBusy = maxRepeatsOnBusy;
        return this;
    }

    public AsyncRequestResult asyncRequest(ProtocolCommand command, String target) {
        return asyncRequest(command, target, null);
    }

    public AsyncRequestResult asyncRequest(ProtocolCommand command, String target, ProtocolParameter parameters) {
        ProtocolMessage message = new ProtocolMessage(command, source, target, role, parameters);
        log(RequestProcessor.class, DEBUG, "Async Request: " + message);
        return new AsyncRequestResult(connector, busyReplies, maxRepeatsOnBusy, message);
    }

    public SyncRequestResult syncRequest(ProtocolCommand command, String target) {
        return syncRequest(command, target, null);
    }

    public SyncRequestResult syncRequest(ProtocolCommand command, String target, ProtocolParameter parameters) {
        ProtocolMessage message = new ProtocolMessage(command, source, target, role, parameters);
        log(RequestProcessor.class, DEBUG, "Sync Request: " + message);
        return new SyncRequestResult(connector, busyReplies, maxRepeatsOnBusy, message);
    }

    public void asyncBroadcast(ProtocolCommand command) {
        asyncBroadcast(command, null);
    }

    public void asyncBroadcast(ProtocolCommand command, ProtocolParameter parameters) {
        ProtocolMessage message = new ProtocolMessage(command, source, ANY, role, parameters);
        log(RequestProcessor.class, DEBUG, "Broadcast: " + message);
        connector.request(message);
    }

    public RemoteConnector getConnector() {
        return connector;
    }

    public Set<ProtocolCommand> getBusyReplies() {
        return busyReplies;
    }

    public int getMaxRepeatsOnBusy() {
        return maxRepeatsOnBusy;
    }

    private void ping() {
        boolean running = true;
        while (running) {
            try {
                Thread.sleep(SLEEP);
            } catch (InterruptedException e) {
                running = false;
            }
            if (running) {
                connector.getRemoteContainerIds().stream()
                         .filter(id -> System.currentTimeMillis() - connector.getLastPing(id) > SLEEP)
                         .forEach(this::ping);
            }
        }
    }

    private void ping(String remoteContainerId) {
        asyncRequest(hey, remoteContainerId).onSuccess(ho, p -> log(RequestProcessor.class, DEBUG, "Ping done"))
                                            .onFail(p -> log(RequestProcessor.class, DEBUG, "Ping failed"))
                                            .process();
    }

    @Override
    public void close() {
        if (connector != null) {
            connector.close();
        }

        if (pingThread != null) {
            pingThread.interrupt();
        }
    }
}
