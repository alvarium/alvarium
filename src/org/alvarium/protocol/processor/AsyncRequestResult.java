/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.processor;

import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.RemoteConnector;
import org.alvarium.protocol.command.ProtocolCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.invalidMessage;
import static org.alvarium.protocol.processor.FailResult.FailReason.*;

public class AsyncRequestResult {
    private ProtocolMessage requestMessage;

    private Consumer<FailResult> failDelegate;
    private final Map<ProtocolCommand, Consumer<SuccessResult>> successDelegates = new HashMap<>();

    private int maxRepeatsOnBusy = 10;
    private Set<ProtocolCommand> busyReplies;

    private RemoteConnector connector;

    public AsyncRequestResult(RemoteConnector connector, Set<ProtocolCommand> busyReplies, int maxRepeatsOnBusy,
                              ProtocolMessage requestMessage) {
        this.connector = connector;
        this.busyReplies = busyReplies;
        this.maxRepeatsOnBusy = maxRepeatsOnBusy;
        this.requestMessage = requestMessage;
    }

    public AsyncRequestResult onFail(Consumer<FailResult> failDelegate) {
        this.failDelegate = failDelegate;
        return this;
    }

    public AsyncRequestResult onSuccess(List<ProtocolCommand> commands, Consumer<SuccessResult> successDelegate) {
        for (ProtocolCommand command : commands) {
            successDelegates.put(command, successDelegate);
        }
        return this;
    }

    public AsyncRequestResult onSuccess(ProtocolCommand command, Consumer<SuccessResult> successDelegate) {
        successDelegates.put(command, successDelegate);
        return this;
    }

    public AsyncRequestResult setMaxRepeatsOnBusy(int maxRepeatsOnBusy) {
        this.maxRepeatsOnBusy = maxRepeatsOnBusy;
        return this;
    }

    public void process() {
        process(false);
    }

    public void process(boolean ignoreConnectionValidation) {
        if (ignoreConnectionValidation || connector.isConnected()) {
            boolean running = true;
            int tries = 0;

            while (running && tries < maxRepeatsOnBusy) {
                tries++;
                ProtocolMessage reply = connector.request(requestMessage);
                if (successDelegates.containsKey(reply.getProtocolCommand())) {
                    fireSuccess(new SuccessResult(requestMessage, reply));
                    running = false;
                } else if (busyReplies.contains(reply.getProtocolCommand())) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        fireFail(new FailResult(busy, requestMessage, invalidMessage));
                        running = false;
                    }
                } else {
                    fireFail(new FailResult(invalidCommand, requestMessage, reply));
                }
            }
        } else {
            fireFail(new FailResult(notConnected, requestMessage, invalidMessage));
        }
    }

    private void fireSuccess(SuccessResult result) {
        Consumer<SuccessResult> successDelegate = successDelegates.get(result.getReplyMessage().getProtocolCommand());
        if (successDelegate != null) {
            log(AsyncRequestResult.class, DEBUG, "Fire Success: " + result);
            successDelegate.accept(result);
        } else {
            fireFail(new FailResult(invalidCommand, result.getRequestMessage(), result.getReplyMessage()));
        }
    }

    private void fireFail(FailResult result) {
        if (failDelegate != null) {
            log(AsyncRequestResult.class, DEBUG, "Fire Fail: " + result);
            failDelegate.accept(result);
        } else {
            log(AsyncRequestResult.class, DEBUG, "Nobody to fire fail to! Fire Fail: " + result);
        }
    }
}
