/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.processor;

import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.RemoteConnector;
import org.alvarium.protocol.command.ProtocolCommand;

import java.util.HashSet;
import java.util.Set;

import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.invalidMessage;
import static org.alvarium.protocol.processor.FailResult.FailReason.*;

public class SyncRequestResult {
    private ProtocolMessage requestMessage;

    private int maxRepeatsOnBusy = 10;
    private Set<ProtocolCommand> busyReplies;
    private final Set<ProtocolCommand> validCommands = new HashSet<>();

    private RemoteConnector connector;

    public SyncRequestResult(RemoteConnector connector, Set<ProtocolCommand> busyReplies, int maxRepeatsOnBusy,
                             ProtocolMessage requestMessage) {
        this.connector = connector;
        this.busyReplies = busyReplies;
        this.maxRepeatsOnBusy = maxRepeatsOnBusy;
        this.requestMessage = requestMessage;
    }

    public SyncRequestResult addValidReply(ProtocolCommand command) {
        validCommands.add(command);
        return this;
    }

    public SyncRequestResult setMaxRepeatsOnBusy(int maxRepeatsOnBusy) {
        this.maxRepeatsOnBusy = maxRepeatsOnBusy;
        return this;
    }

    public ProcessingResult process() {
        return process(false);
    }

    public ProcessingResult process(boolean ignoreConnectionValidation) {
        ProcessingResult result = null;
        if (ignoreConnectionValidation || connector.isConnected()) {
            boolean running = true;
            int tries = 0;

            while (running && tries < maxRepeatsOnBusy) {
                tries++;
                ProtocolMessage reply = connector.request(requestMessage);
                if (validCommands.contains(reply.getProtocolCommand())) {
                    result = new SuccessResult(requestMessage, reply);
                    running = false;
                } else if (busyReplies.contains(reply.getProtocolCommand())) {
                    result = new FailResult(busy, requestMessage, invalidMessage);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        result = new FailResult(busy, requestMessage, invalidMessage);
                        running = false;
                    }
                } else {
                    result = new FailResult(invalidCommand, requestMessage, reply);
                    if (reply.getProtocolCommand() == invalidMessage.getProtocolCommand()) {
                        running = false;
                    }
                }
            }
        } else {
            result = new FailResult(notConnected, requestMessage, invalidMessage);
        }

        result = result == null ? new FailResult(invalidCommand, requestMessage, invalidMessage) : result;
        log(SyncRequestResult.class, DEBUG, "Process result: " + result);
        return result;
    }
}
