/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol;

import org.alvarium.protocol.command.GenericProtocolCommands;
import org.alvarium.protocol.command.ProtocolCommand;
import org.alvarium.protocol.parameters.ProtocolParameter;

import static org.alvarium.protocol.command.GenericProtocolCommands.obuzz_rtfm;
import static org.alvarium.protocol.ProtocolRole.mesh;
import static org.alvarium.protocol.ProtocolRole.unknown;

public class ProtocolMessage {
    public static final String ANY = "*";

    public static final ProtocolMessage invalidMessage = new ProtocolMessage(obuzz_rtfm, ANY, ANY, unknown);
    public static final ProtocolMessage broadcastResult = new ProtocolMessage(GenericProtocolCommands.broadcastResult, ANY, ANY, mesh);

    private ProtocolCommand protocolCommand;
    private String source;
    private String target;
    private ProtocolRole role;
    private ProtocolParameter protocolParameter;

    public ProtocolMessage(ProtocolCommand protocolCommand, String source, String target, ProtocolRole role,
                           ProtocolParameter protocolParameter) {
        this.protocolCommand = protocolCommand;
        this.source = source;
        this.target = target;
        this.role = role;
        this.protocolParameter = protocolParameter;
    }

    public ProtocolMessage(ProtocolCommand protocolCommand, String source, String target, ProtocolRole role) {
        this(protocolCommand, source, target, role, null);
    }

    public ProtocolCommand getProtocolCommand() {
        return protocolCommand;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public ProtocolRole getSourceRole() {
        return role;
    }

    public ProtocolParameter getProtocolParameter() {
        return protocolParameter;
    }

    @Override
    public String toString() {
        return "ProtocolMessage{" +
                "protocolCommand=" + protocolCommand +
                ", source='" + source + '\'' +
                ", target='" + target + '\'' +
                ", role=" + role +
                ", protocolParameters=" + protocolParameter +
                '}';
    }
}
