/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.handler;

import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.ProtocolRole;
import org.alvarium.protocol.command.ProtocolCommand;
import org.alvarium.protocol.parameters.ProtocolParameter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.command.GenericProtocolCommands.buzzy;
import static org.alvarium.protocol.command.GenericProtocolCommands.obuzz_rtfm;

public class ProtocolHandler {
    private ProtocolRole role;
    private String containerId;
    private boolean initialised = false;

    private Function<ProtocolMessage, ProtocolMessage> defaultHandler;
    private Map<ProtocolCommand, Function<ProtocolMessage, ProtocolMessage>> commandHandlers = new HashMap<>();

    public ProtocolHandler(String containerId, ProtocolRole role) {
        this.containerId = containerId;
        this.role = role;
    }

    public ProtocolHandler setInitialised(boolean flag) {
        this.initialised = flag;
        return this;
    }

    public ProtocolHandler registerDefault(Function<ProtocolMessage, ProtocolMessage> handler) {
        defaultHandler = handler;
        return this;
    }

    public ProtocolHandler register(ProtocolCommand command, Function<ProtocolMessage, ProtocolMessage> handler) {
        commandHandlers.put(command, handler);
        return this;
    }

    public ProtocolHandler register(List<ProtocolCommand> commands,
                                    Function<ProtocolMessage, ProtocolMessage> handler) {
        for (ProtocolCommand command : commands) {
            commandHandlers.put(command, handler);
        }
        return this;
    }

    public boolean isMessageSupported(ProtocolMessage message) {
        return ProtocolRole.isCompatible(message.getSourceRole(), role);
    }

    public ProtocolMessage handle(ProtocolMessage message) {
        log(ProtocolHandler.class, DEBUG, "Handle: " + message);
        if (message == null) {
            return message(obuzz_rtfm, ANY);
        }

        if (!initialised) {
            return message(buzzy, message.getSource());
        }

        Function<ProtocolMessage, ProtocolMessage> handler = commandHandlers.get(message.getProtocolCommand());
        if (handler == null) {
            if (defaultHandler == null) {
                throw new IllegalArgumentException("No valid handler found. Default handler is null");
            }
            handler = defaultHandler;
        }
        return handler.apply(message);
    }

    public ProtocolMessage message(ProtocolCommand command, String target) {
        ProtocolMessage message = new ProtocolMessage(command, containerId, target, role);
        log(ProtocolHandler.class, DEBUG, "Message: " + message);
        return message;
    }

    public ProtocolMessage message(ProtocolCommand command, String target, ProtocolParameter parameters) {
        ProtocolMessage message = new ProtocolMessage(command, containerId, target, role, parameters);
        log(ProtocolHandler.class, DEBUG, "Message: " + message);
        return message;
    }
}
