/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol.parameters;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.datatypes.TopicKey;
import org.alvarium.container.manager.ChangeEvent;
import org.alvarium.container.manager.ComponentType;

@ClassDependencies(primitiveRegisters = {ChangeEvent.Type.class, ComponentManagerChangeEvent.Source.class,
                                                ComponentType.class, ComponentId.class, TcpAddress.class, TopicKey.class, TopicEncodingKey.class})
public class ComponentManagerChangeEvent extends ProtocolParameter {
    public enum Source {
        host,
        encoding,
        component
    }

    private Source eventSource;
    private ChangeEvent.Type type;
    private String componentName;
    private ComponentType componentType;
    private String containerId;
    private ComponentId componentId;
    private TcpAddress tcpAddress;
    private TopicKey topicKey;
    private TopicEncodingKey encoding;

    public ComponentManagerChangeEvent() {
    }

    public ComponentManagerChangeEvent(Source source, ChangeEvent changeEvent) {
        this.eventSource = source;
        this.type = changeEvent.getType();
        this.componentName = changeEvent.getComponentName();
        this.componentType = changeEvent.getComponentType();
        this.containerId = changeEvent.getContainerId();
        this.componentId = changeEvent.getComponentId();
        this.tcpAddress = changeEvent.getTcpAddress();
        this.topicKey = changeEvent.getTopicKey();
        this.encoding = changeEvent.getEncoding();
    }

    public Source getEventSource() {
        return eventSource;
    }

    public ChangeEvent.Type getType() {
        return type;
    }

    public String getComponentName() {
        return componentName;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public String getContainerId() {
        return containerId;
    }

    public TcpAddress getTcpAddress() {
        return tcpAddress;
    }

    public TopicKey getTopicKey() {
        return topicKey;
    }

    public TopicEncodingKey getEncoding() {
        return encoding;
    }

    @Override
    public String toString() {
        return "ComponentManagerChangeEvent{" +
               "eventSource=" + eventSource +
               ", type=" + type +
               ", componentName='" + componentName + '\'' +
               ", componentType=" + componentType +
               ", containerId='" + containerId + '\'' +
               ", componentId=" + componentId +
               ", tcpAddress=" + tcpAddress +
               ", topicKey=" + topicKey +
               ", encoding=" + encoding +
               '}';
    }
}
