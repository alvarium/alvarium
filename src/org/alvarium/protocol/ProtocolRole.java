/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.protocol;

public enum ProtocolRole {
    mesh((byte) 1),
    admin((byte) 2),
    any((byte) 3),
    unknown((byte) 0);

    private byte representation;

    ProtocolRole(byte representation) {
        this.representation = representation;
    }

    public byte getValue() {
        return representation;
    }

    public static ProtocolRole fromValue(byte value) {
        switch (value) {
            case 1:
                return mesh;
            case 2:
                return admin;
            case 3:
                return any;
            default:
                return unknown;
        }
    }

    @Override
    public String toString() {
        return name();
    }

    public static boolean isCompatible(ProtocolRole a, ProtocolRole b) {
        return a == any || b == any || a == b;
    }
}
