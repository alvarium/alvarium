/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.datatypes;

public class ComponentId {
    public static final String SEPARATOR = "::";

    private String containerId;
    private String componentId;

    public ComponentId() {
    }

    public ComponentId(String containerId, String componentId) {
        this.containerId = containerId;
        this.componentId = componentId;
    }

    public String getContainerId() {
        return containerId;
    }

    public String getComponentId() {
        return componentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentId that = (ComponentId) o;
        return containerId.equals(that.containerId) && componentId.equals(that.componentId);

    }

    @Override
    public int hashCode() {
        int result = containerId.hashCode();
        result = 31 * result + componentId.hashCode();
        return result;
    }

    public String toString() {
        return containerId + SEPARATOR + componentId;
    }

    public static ComponentId create(String serialisation) {
        if (serialisation.contains(SEPARATOR)) {
            String[] parts = serialisation.split(SEPARATOR);
            return new ComponentId(parts[0], parts[1]);
        }
        throw new IllegalArgumentException("Invalid format for component key. Should be <containerId>::<componentId>");
    }
}
