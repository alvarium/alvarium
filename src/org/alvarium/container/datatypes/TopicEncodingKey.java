/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.datatypes;

import java.util.Arrays;
import java.util.Random;

public class TopicEncodingKey {
    public final static int HEADER_SIZE = 3;
    public final static int ENCODING_SIZE = HEADER_SIZE + 3;

    private static final Random rnd = new Random(System.currentTimeMillis());

    private static TopicEncodingKey lastTopicEncodingKey = null;
    private static byte[] header = null;

    protected byte[] key;

    public TopicEncodingKey() {
    }

    public TopicEncodingKey(byte[] key) {
        this.key = key;
    }

    public byte[] getKey() {
        return key;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopicEncodingKey that = (TopicEncodingKey) o;
        return Arrays.equals(key, that.key);
    }

    public int hashCode() {
        return Arrays.hashCode(key);
    }

    @Override
    public String toString() {
        return Arrays.toString(key);
    }

    public static void init(byte[] header) {
        TopicEncodingKey.header = header;
        byte[] encoding = new byte[ENCODING_SIZE];
        System.arraycopy(header, 0, encoding, 0, HEADER_SIZE);
        for (int i = HEADER_SIZE; i < ENCODING_SIZE; i++) {
            encoding[i] = 0;
        }
        lastTopicEncodingKey = new TopicEncodingKey(encoding);
    }

    public static TopicEncodingKey createNewEncodingKey() {
        byte[] encoding = new byte[ENCODING_SIZE];
        System.arraycopy(lastTopicEncodingKey.key, 0, encoding, 0, lastTopicEncodingKey.key.length);

        TopicEncodingKey result = new TopicEncodingKey(encoding);

        boolean ok = false;
        int i = HEADER_SIZE;
        while (!ok && i < ENCODING_SIZE) {
            if (result.key[i] == 255) {
                result.key[i] = 0;
                i++;
            } else {
                result.key[i]++;
                ok = true;
            }
        }
        lastTopicEncodingKey = result;
        return result;
    }

    public static byte[] generateRandomHeader() {
        byte[] result = new byte[HEADER_SIZE];
        rnd.nextBytes(result);
        return result;
    }

    public static byte[] getHeader() {
        return header;
    }
}