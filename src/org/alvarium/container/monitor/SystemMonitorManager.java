/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.monitor;

import org.alvarium.component.base.Closeable;
import org.alvarium.container.datatypes.ComponentId;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SystemMonitorManager implements Closeable {
    //todo; this component is currently local within the container. Check if it needs to be distributed
    public static final SystemMonitorManager systemMonitorManager = new SystemMonitorManager();

    private final List<MonitorChangeListener> changeListeners = new LinkedList<>();
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    public SystemMonitorManager() {
        addListener(ContainerMonitor.instance);
    }

    public void addListener(MonitorChangeListener listener) {
        synchronized (changeListeners) {
            changeListeners.add(listener);
        }
    }

    public void removeListener(MonitorChangeListener listener) {
        synchronized (changeListeners) {
            changeListeners.remove(listener);
        }
    }

    private void fireComponentChange(MonitorChangeEvent event) {
        synchronized (changeListeners) {
            for (MonitorChangeListener listener : changeListeners) {
                executorService.execute(() -> listener.componentEventFired(event));
            }
        }
    }

    private void fireContainerChange(MonitorChangeEvent event) {
        synchronized (changeListeners) {
            for (MonitorChangeListener listener : changeListeners) {
                executorService.execute(() -> listener.containerEventFired(event));
            }
        }
    }

    public void fireAct(ComponentId componentId) {
        fireComponentChange(new MonitorChangeEvent(componentId, MonitorChangeEvent.EventType.act));
    }

    public void fireComponentStarted(ComponentId componentId) {
        fireComponentChange(new MonitorChangeEvent(componentId, MonitorChangeEvent.EventType.started));
    }

    public void fireComponentClosed(ComponentId componentId) {
        fireComponentChange(new MonitorChangeEvent(componentId, MonitorChangeEvent.EventType.closed));
    }

    public void fireContainerStarted(String containerId) {
        fireContainerChange(new MonitorChangeEvent(containerId, MonitorChangeEvent.EventType.started));
    }

    public void fireContainerClosed(String containerId) {
        fireContainerChange(new MonitorChangeEvent(containerId, MonitorChangeEvent.EventType.closed));
    }

    @Override
    public void close() {
        removeListener(ContainerMonitor.instance);
    }
}
