/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.xml;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "service")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceXmlConfig {
    @XmlAttribute(name = "id", required = true)
    private String id;
    @XmlAttribute(name = "name", required = true)
    private String name;
    @XmlAttribute(name = "class", required = true)
    private String serviceClass;
    @XmlAttribute(name = "ioThreads", required = false)
    private Integer ioThreads;
    @XmlElement(name = "property", nillable = false, required = false)
    private List<PropertyXmlConfig> propertyConfigs = new LinkedList<>();

    public ServiceXmlConfig() {
    }

    public ServiceXmlConfig(String id, String name, String serviceClass, Integer ioThreads,
                            List<PropertyXmlConfig> propertyConfigs) {
        this.id = id;
        this.name = name;
        this.serviceClass = serviceClass;
        this.ioThreads = ioThreads;
        this.propertyConfigs = propertyConfigs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    public Integer getIoThreads() {
        return ioThreads;
    }

    public void setIoThreads(Integer ioThreads) {
        this.ioThreads = ioThreads;
    }

    public List<PropertyXmlConfig> getPropertyConfigs() {
        return propertyConfigs;
    }

    public void setPropertyConfigs(List<PropertyXmlConfig> propertyConfigs) {
        this.propertyConfigs = propertyConfigs;
    }

    @Override
    public String toString() {
        return "ServiceXmlConfig{" +
               "id='" + id + '\'' +
               ", name='" + name + '\'' +
               ", serviceClass='" + serviceClass + '\'' +
               ", ioThreads=" + ioThreads +
               ", propertyConfigs=" + propertyConfigs +
               '}';
    }
}
