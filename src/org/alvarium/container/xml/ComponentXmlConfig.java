/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.xml;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "component")
@XmlAccessorType(XmlAccessType.FIELD)
public class ComponentXmlConfig {
    @XmlAttribute(name = "componentId", required = true)
    private String id;
    @XmlAttribute(name = "componentClass", required = true)
    private String componentClass;
    @XmlElement(name = "scheduler", nillable = true, required = false)
    private SchedulerPropertyXmlConfig scheduler;
    @XmlElement(name = "subscribe", nillable = false, required = false)
    private List<SubscriptionXmlConfig> subscriptionConfigs = new LinkedList<>();
    @XmlElement(name = "publish", nillable = false, required = false)
    private List<PublishXmlConfig> publishConfigs = new LinkedList<>();
    @XmlElement(name = "property", nillable = false, required = false)
    private List<PropertyXmlConfig> propertyConfigs = new LinkedList<>();

    public ComponentXmlConfig() {
    }

    public ComponentXmlConfig(String componentId, String componentClass, SchedulerPropertyXmlConfig scheduler,
                              List<SubscriptionXmlConfig> subscriptionConfigs,
                              List<PublishXmlConfig> publishConfigs,
                              List<PropertyXmlConfig> propertyConfigs) {
        this.id = componentId;
        this.componentClass = componentClass;
        this.scheduler = scheduler;
        this.subscriptionConfigs = subscriptionConfigs;
        this.publishConfigs = publishConfigs;
        this.propertyConfigs = propertyConfigs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComponentClass() {
        return componentClass;
    }

    public void setComponentClass(String componentClass) {
        this.componentClass = componentClass;
    }

    public SchedulerPropertyXmlConfig getScheduler() {
        return scheduler;
    }

    public void setScheduler(SchedulerPropertyXmlConfig scheduler) {
        this.scheduler = scheduler;
    }

    public List<SubscriptionXmlConfig> getSubscriptionConfigs() {
        return subscriptionConfigs;
    }

    public void setSubscriptionConfigs(List<SubscriptionXmlConfig> subscriptionConfigs) {
        this.subscriptionConfigs = subscriptionConfigs;
    }

    public List<PublishXmlConfig> getPublishConfigs() {
        return publishConfigs;
    }

    public void setPublishConfigs(List<PublishXmlConfig> publishConfigs) {
        this.publishConfigs = publishConfigs;
    }

    public List<PropertyXmlConfig> getPropertyConfigs() {
        return propertyConfigs;
    }

    public void setPropertyConfigs(List<PropertyXmlConfig> propertyConfigs) {
        this.propertyConfigs = propertyConfigs;
    }

    @Override
    public String toString() {
        return "component{" +
                "componentId='" + id + '\'' +
                ", componentClass='" + componentClass + '\'' +
                ", scheduler='" + scheduler + '\'' +
                ", subscriptionConfigs=" + subscriptionConfigs +
                ", publishConfigs=" + publishConfigs +
                ", propertyConfigs=" + propertyConfigs +
                '}';
    }
}
