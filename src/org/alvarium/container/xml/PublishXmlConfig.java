/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "publish")
@XmlAccessorType(XmlAccessType.FIELD)
public class PublishXmlConfig {
    @XmlAttribute(name = "topic", required = true)
    private String topic;
    @XmlAttribute(name = "internalTopic", required = true)
    private String internalTopic;

    public PublishXmlConfig() {
    }

    public PublishXmlConfig(String topic, String internalTopic) {
        this.topic = topic;
        this.internalTopic = internalTopic;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getInternalTopic() {
        return internalTopic;
    }

    public void setInternalTopic(String internalTopic) {
        this.internalTopic = internalTopic;
    }

    @Override
    public String toString() {
        return "publish{" +
                "topic='" + topic + '\'' +
                ", internalTopic='" + internalTopic + '\'' +
                '}';
    }
}
