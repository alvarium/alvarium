/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.xml;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContainerXmlConfig {
    @XmlAttribute(name = "containerId", required = true)
    private String containerId;
    @XmlElementWrapper(name = "components")
    @XmlElement(name = "component")
    private List<ComponentXmlConfig> componentConfigs = new LinkedList<>();
    @XmlElementWrapper(name = "schedulers")
    @XmlElement(name = "scheduler")
    private List<SchedulerXmlConfig> schedulerConfigs = new LinkedList<>();

    @XmlElementWrapper(name = "services")
    @XmlElement(name = "service")
    private List<ServiceXmlConfig> serviceConfigs = new LinkedList<>();

    public ContainerXmlConfig() {
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public List<ComponentXmlConfig> getComponentConfigs() {
        return componentConfigs;
    }

    public void setComponentConfigs(List<ComponentXmlConfig> componentConfigs) {
        this.componentConfigs = componentConfigs;
    }

    public List<SchedulerXmlConfig> getSchedulerConfigs() {
        return schedulerConfigs;
    }

    public void setSchedulerConfigs(List<SchedulerXmlConfig> schedulerConfigs) {
        this.schedulerConfigs = schedulerConfigs;
    }

    public List<ServiceXmlConfig> getServiceConfigs() {
        return serviceConfigs;
    }

    public void setServiceConfigs(List<ServiceXmlConfig> serviceConfigs) {
        this.serviceConfigs = serviceConfigs;
    }

    @Override
    public String toString() {
        return "container{" +
                "containerId='" + containerId + '\'' +
                ", componentConfigs=" + componentConfigs +
                ", schedulerConfigs=" + schedulerConfigs +
                ", serviceConfigs=" + serviceConfigs +
                '}';
    }
}
