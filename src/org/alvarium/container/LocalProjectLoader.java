/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container;

import org.alvarium.container.communication.MeshManager;
import org.alvarium.container.communication.TerminalProtocolManager;
import org.alvarium.container.exception.ComponentStartupException;
import org.alvarium.container.model.ProjectConfig;
import org.alvarium.container.xml.ProjectManager;
import org.alvarium.container.xml.ProjectXmlConfig;
import org.alvarium.logger.Logger;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.parameters.ProjectRelatedParameter;
import org.alvarium.terminal.launcher.ContainerLauncher;
import org.alvarium.validation.*;

import java.io.File;

import static org.alvarium.container.model.GenericConverter.genericConverter;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.ProtocolRole.mesh;
import static org.alvarium.protocol.command.AdminProtocolCommands.adminLoadProject;
import static org.alvarium.protocol.command.AdminProtocolCommands.adminLoadProjectAck;

public class LocalProjectLoader {
    private static final ModelValidationManager modelValidationManager = new ModelValidationManager();

    static {
        modelValidationManager.addValidator(new ComponentsValidation());
        modelValidationManager.addValidator(new LinkValidation());
        modelValidationManager.addValidator(new ComponentParametersValidation());
        modelValidationManager.addValidator(new ComponentDataTypeValidation());
    }

    public static boolean loadConfigFile(RunParams params, MeshManager meshManager) {
        File configFilename = new File(params.projectPath);
        if (!configFilename.canRead()) {
            log(ContainerLauncher.class, Logger.Level.CRITICAL, "File not found: " + configFilename.getAbsolutePath());
            return false;
        }
        ProjectXmlConfig projectXmlConfig = ProjectManager.load(configFilename);
        ModelValidationResult validationResult = modelValidationManager.checkComponentModel(projectXmlConfig, false);
        if (validationResult.hasErrors()) {
            for (String line : validationResult.getErrorList()) {
                log(ContainerLauncher.class, Logger.Level.INFORM, "Error: " + line);
            }
            log(ContainerLauncher.class, Logger.Level.CRITICAL, "Too manny errors while executing load. Aborted!");
            return false;
        }

        if (!params.ignoreWarnings) {
            if (validationResult.hasWarnings()) {
                for (String line : validationResult.getWarningList()) {
                    log(ContainerLauncher.class, Logger.Level.INFORM, "Warning: " + line);
                }
                if (!params.noExitOnWarnings) {
                    log(ContainerLauncher.class, Logger.Level.CRITICAL, "Too manny warnings while executing load. Aborted!");
                    return false;
                }
            }
        }

        ProjectConfig config;
        try {
            config = genericConverter.convert(projectXmlConfig, false);
        } catch (ComponentStartupException e) {
            e.printStackTrace();
            log(ContainerLauncher.class, Logger.Level.CRITICAL, "Got exception: " + e.getCause().getMessage());
            log(ContainerLauncher.class, Logger.Level.CRITICAL, "ComponentID: " + e.getComponentId());
            log(ContainerLauncher.class, Logger.Level.CRITICAL, "Cannot start platform ... exiting the current instance !");
            return false;
        }
        if (config != null) {
            log(ContainerLauncher.class, Logger.Level.INFORM, "Sending load message, waiting for reply");

            TerminalProtocolManager tpManager = meshManager.getTerminalProtocolManager();

            ProtocolMessage message = new ProtocolMessage(adminLoadProject, tpManager.getContainerId(), ANY, mesh,
                                                          new ProjectRelatedParameter(config));
            ProtocolMessage reply = tpManager.getProtocolHandler().handle(message);

            log(ContainerLauncher.class, Logger.Level.INFORM, "Reply received: " + reply);
            return reply != null && reply.getProtocolCommand() == adminLoadProjectAck;
        } else {
            return false;
        }
    }
}
