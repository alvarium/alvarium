/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container;

import org.alvarium.component.base.AbstractComponent;
import org.alvarium.component.service.Scheduler;
import org.alvarium.component.service.Service;
import org.alvarium.container.communication.NetworkManager;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.exception.ComponentStartupException;
import org.alvarium.container.model.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.container.monitor.SystemMonitorManager.systemMonitorManager;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

public class ContainerUtils {
    public static void closeLocalContainer(String containerId) {
        componentManager.closeAllComponents();
        systemMonitorManager.fireContainerClosed(containerId);
    }

    public static boolean loadLocalContainer(String containerId, ContainerConfig containerConfig,
                                             NetworkManager networkManager) {
        if (containerConfig != null && containerConfig.getContainerId() != null &&
            containerId.equals(containerConfig.getContainerId())) {
            for (SchedulerConfig config : containerConfig.getSchedulerConfigs()) {
                if (!startScheduler(containerId, config, networkManager)) {
                    return false;
                }
            }

            for (ServiceConfig config : containerConfig.getServiceConfigs()) {
                if (!startService(containerId, config, networkManager)) {
                    return false;
                }
            }

            for (ComponentConfig componentConfig : containerConfig.getComponentConfigs()) {
                AbstractComponent component = startComponent(containerId, componentConfig, networkManager);
                if (component != null) {
                    publish(component, componentConfig.getPublishConfigs());
                    subscribeToScheduler(component, componentConfig.getSchedulerId());
                } else {
                    return false;
                }
            }

            for (ComponentConfig componentConfig : containerConfig.getComponentConfigs()) {
                subscribe(componentConfig.getComponentId(), componentConfig.getSubscriptionConfigs());
            }

            systemMonitorManager.fireContainerStarted(containerId);
            return true;
        } else {
            return false;
        }
    }

    public static boolean startScheduler(String containerId, SchedulerConfig config, NetworkManager networkManager) {
        if (config != null) {
            ComponentId componentId = config.getComponentId();
            if (componentId != null && componentId.getContainerId() != null
                && componentId.getContainerId().equals(containerId)) {
                if (config.getAddress() == null) {
                    TcpAddress freeAddress = networkManager.findRandomFreeAddress();
                    componentManager.addHost(componentId, freeAddress);
                    config.setAddress(freeAddress);
                }

                Scheduler scheduler = new Scheduler(config);
                componentManager.addScheduler(scheduler);
                scheduler.start();
                log(ContainerUtils.class, DEBUG, "Scheduler started! Id = " + componentId);
                return true;
            }
        }
        return false;
    }

    public static boolean startService(String containerId, ServiceConfig config, NetworkManager networkManager) {
        if (config != null) {
            ComponentId componentId = config.getComponentId();
            if (componentId != null && componentId.getContainerId() != null
                && componentId.getContainerId().equals(containerId)) {
                if (config.getAddress() == null) {
                    TcpAddress freeAddress = networkManager.findRandomFreeAddress();
                    componentManager.addHost(componentId, freeAddress);
                    config.setAddress(freeAddress);
                }

                if (config.getServiceClass() != null) {
                    try {
                        Class<Service> clazz = convertClass(componentId, config.getServiceClass());
                        Constructor constructor = clazz.getConstructor(ComponentConfig.class);
                        constructor.setAccessible(true);
                        Service service = (Service) constructor.newInstance(config);
                        componentManager.addService(service);
                        service.start();
                        log(ContainerUtils.class, DEBUG, "Service started! Id = " + componentId);
                        return true;
                    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException |
                                     InvocationTargetException e) {
                        throw new ComponentStartupException(componentId.toString(), e);
                    }
                }
            }
        }
        return false;
    }

    public static AbstractComponent startComponent(String containerId, ComponentConfig config,
                                                   NetworkManager networkManager) {
        if (config != null) {
            ComponentId componentId = config.getComponentId();
            if (componentId != null && componentId.getContainerId() != null
                && componentId.getContainerId().equals(containerId)) {
                if (config.getAddress() == null) {
                    TcpAddress freeAddress = networkManager.findRandomFreeAddress();
                    componentManager.addHost(config.getComponentId(), freeAddress);
                    config.setAddress(freeAddress);
                }

                try {
                    if (config.getComponentClass() != null) {
                        Class<AbstractComponent> componentClass = convertClass(componentId, config.getComponentClass());
                        Constructor constructor = componentClass.getConstructor(ComponentConfig.class);
                        constructor.setAccessible(true);
                        AbstractComponent component = (AbstractComponent) constructor.newInstance(config);
                        component.setupComponentConfig(config.getPropertyConfigs());
                        componentManager.addComponent(component);
                        log(ContainerUtils.class, DEBUG, "Component started! Id = " + componentId);
                        return component;
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException |
                                 InvocationTargetException e) {
                    throw new ComponentStartupException(componentId.toString(), e);
                }
            }
        }
        return null;
    }

    //todo; add this to the admin protocol
    public static boolean removeComponent(ComponentId componentId) {
        if (componentId != null) {
            componentManager.removeComponent(componentId);
            log(ContainerUtils.class, DEBUG, "Component removed! Id = " + componentId);
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> Class<T> convertClass(ComponentId componentId, String className) {
        try {
            Class clazz = Class.forName(className);
            if (clazz != null) {
                return (Class<T>) Class.forName(className);
            } else {
                throw new ComponentStartupException(componentId.toString());
            }
        } catch (ClassNotFoundException e) {
            throw new ComponentStartupException(componentId.toString(), e);
        }
    }

    //todo; add this into the terminal protocol
    public static void subscribeToScheduler(ComponentId subscriber, ComponentId schedulerId) {
        if (subscriber != null && schedulerId != null) {
            AbstractComponent component = componentManager.getComponent(subscriber);
            subscribeToScheduler(component, schedulerId);
        }
    }

    private static void subscribeToScheduler(AbstractComponent component, ComponentId schedulerId) {
        if (component != null && schedulerId != null) {
            if (componentManager.isResolvable(schedulerId)) {
                component.subscribeToScheduler(schedulerId);
                log(ContainerUtils.class, DEBUG, "Scheduler subscription successful! " +
                                                 "ComponentId = %s, SchedulerId = %s",
                    component.getComponentId(), schedulerId);
            } else {
                componentManager.queueManager.postponeSchedulerSubscription(component.getComponentId(), schedulerId);
            }
        }
    }

    //todo; add this into the terminal protocol
    public static void unsubscribeFromScheduler(ComponentId subscriber, ComponentId schedulerId) {
        if (subscriber != null && schedulerId != null) {
            AbstractComponent component = componentManager.getComponent(subscriber);
            unsubscribeFromScheduler(component, schedulerId);
        }
    }

    private static void unsubscribeFromScheduler(AbstractComponent component, ComponentId schedulerId) {
        if (component != null && schedulerId != null) {
            component.unsubscribeFromScheduler(schedulerId);
            log(ContainerUtils.class, DEBUG, "Scheduler unsubscribe successful! ComponentId = %s, SchedulerId = %s",
                component.getComponentId(), schedulerId);
        }
    }

    //todo; add this into the terminal protocol
    public static void subscribe(ComponentId subscriber, List<SubscriptionConfig> subscriptionList) {
        if (subscriber != null && subscriptionList != null) {
            AbstractComponent component = componentManager.getComponent(subscriber);
            for (SubscriptionConfig subscription : subscriptionList) {
                subscribe(component, subscription);
            }
        }
    }

    //todo; add this into the terminal protocol
    public static void subscribe(ComponentId subscriber, SubscriptionConfig subscription) {
        if (subscriber != null && subscription != null) {
            AbstractComponent component = componentManager.getComponent(subscriber);
            subscribe(component, subscription);
        }
    }

    private static void subscribe(AbstractComponent subscriber, SubscriptionConfig subscription) {
        if (subscriber != null && subscription != null) {
            if (componentManager.isResolvable(subscription.getComponentId(), subscription.getTopic())) {
                subscriber.subscribe(subscription.getTopic(), subscription.getComponentId(), subscriber);
                log(ContainerUtils.class, DEBUG, "Component subscription successful! " +
                                                 "ComponentId = %s, subscription = %s",
                    subscriber.getComponentId(), subscription);
            } else {
                componentManager.queueManager.postponeComponentSubscription(subscriber.getComponentId(), subscription);
            }
        }
    }

    //todo; add this to the admin protocol
    public static void unsubscribe(AbstractComponent component, List<SubscriptionConfig> subscriptionList) {
        if (component != null && subscriptionList != null) {
            for (SubscriptionConfig subscription : subscriptionList) {
                component.unsubscribe(subscription.getTopic(), subscription.getComponentId());
                log(ContainerUtils.class, DEBUG, "Component unsubscribe successful! " +
                                                 "ComponentId = %s, subscription = %s",
                    component.getComponentId(), subscription);
            }
        }
    }

    public static void publish(AbstractComponent component, List<PublishConfig> publishList) {
        if (component != null && publishList != null) {
            for (PublishConfig publish : publishList) {
                component.publish(publish.getTopic(), publish.getInternalTopic());
                log(ContainerUtils.class, DEBUG, "Component publish successful! ComponentId = %s, topic = %s",
                    component.getComponentId(), publish);
            }
        }
    }

    //todo; add this to the admin protocol
    public static void unpublish(AbstractComponent component, List<PublishConfig> publishList) {
        if (component != null && publishList != null) {
            for (PublishConfig publish : publishList) {
                component.unpublish(publish.getTopic(), publish.getInternalTopic());
                log(ContainerUtils.class, DEBUG, "Component unpublish successful! ComponentId = %s, topic = %s",
                    component.getComponentId(), publish);
            }
        }
    }
}
