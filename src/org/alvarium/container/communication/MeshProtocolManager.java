/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.communication;

import org.alvarium.component.base.Closeable;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.manager.ChangeEvent;
import org.alvarium.container.manager.ComponentType;
import org.alvarium.container.manager.ManagerChangeListener;
import org.alvarium.protocol.ProtocolException;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.command.ProtocolCommand;
import org.alvarium.protocol.handler.ProtocolHandler;
import org.alvarium.protocol.parameters.*;
import org.alvarium.protocol.processor.FailResult;
import org.alvarium.protocol.processor.ProcessingResult;
import org.alvarium.protocol.processor.RequestProcessor;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.alvarium.container.ContainerUtils.closeLocalContainer;
import static org.alvarium.container.ContainerUtils.loadLocalContainer;
import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.ProtocolRole.mesh;
import static org.alvarium.protocol.command.GenericProtocolCommands.*;
import static org.alvarium.protocol.command.MeshProtocolCommands.*;

public class MeshProtocolManager implements ManagerChangeListener, Closeable {
    public static final int MAX_TURNS = 15;

    private static final String TEMP_NAME = "temp_node_name";

    private String containerId;
    private MeshManager meshManager;
    private NetworkManager networkManager;

    private ProtocolHandler protocolHandler;
    private RequestProcessor requestProcessor;

    public MeshProtocolManager(String containerId, MeshManager meshManager, NetworkManager networkManager) {
        this.containerId = containerId;
        this.meshManager = meshManager;
        this.networkManager = networkManager;

        initHandler();
        initRequester();

        componentManager.addListener(this);
    }

    private void initHandler() {
        protocolHandler = new ProtocolHandler(containerId, mesh);
        protocolHandler.registerDefault(m -> {
            log(MeshProtocolManager.class, INFORM, "Received nothing useful from" + m.getSource() + " ,sending rtfm!");
            return protocolHandler.message(obuzz_rtfm, ANY);
        });
        protocolHandler.register(buzz, this::createSalutReply);
        protocolHandler.register(byebye, m -> {
            log(MeshProtocolManager.class, INFORM, "Received Bye-bye from: " + m.getSource());
            return protocolHandler.message(farewell, m.getSource());
        });
        protocolHandler.register(hey, m -> {
            log(MeshProtocolManager.class, INFORM, "Received hey from: " + m.getSource());
            return protocolHandler.message(ho, m.getSource());
        });
        protocolHandler.register(negRequest, this::createNegotiateMessage);
        protocolHandler.register(negResult, this::createNegotiateResult);
        protocolHandler.register(loadContainer, this::loadContainer);
        protocolHandler.register(rollbackContainer, m -> {
            closeLocalContainer(containerId);
            return protocolHandler.message(rollbackContainerAck, m.getSource());
        });
        protocolHandler.register(componentManagerChanged, this::componentManagerChanged);
    }

    private void initRequester() {
        requestProcessor = new RequestProcessor(containerId, mesh, meshManager).addBusyReply(buzzy);
    }

    public ProtocolHandler getProtocolHandler() {
        return protocolHandler;
    }

    @Override
    public void close() {
        componentManager.removeListener(this);
        if (requestProcessor != null) {
            requestProcessor.close();
        }
    }

    public void initialiseProtocol(TcpAddress remotePeer) {
        if (remotePeer != null) {
            Queue<TcpAddress> connections = new LinkedList<>();
            connections.add(remotePeer);
            Set<TcpAddress> alreadyConnected = new HashSet<>();

            while (!connections.isEmpty()) {
                TcpAddress newConnection = connections.poll();
                log(MeshProtocolManager.class, INFORM, "Connecting to a new mesh node = " + newConnection);
                meshManager.connect(TEMP_NAME, newConnection);
                ProcessingResult result = requestProcessor.syncRequest(buzz, TEMP_NAME)
                                                          .addValidReply(buzzEcho).addValidReply(obuzzName)
                                                          .setMaxRepeatsOnBusy(1).process(true);
                if (result instanceof FailResult) {
                    if (((FailResult) result).getFailReason() == FailResult.FailReason.busy) {
                        meshManager.remove(TEMP_NAME);
                        connections.offer(newConnection);
                    } else {
                        log(MeshProtocolManager.class, INFORM, "Received an invalid response. Ignoring it.");
                    }
                } else {
                    ProtocolCommand replyCommand = result.getReplyMessage().getProtocolCommand();
                    if (replyCommand == buzzEcho) {
                        ProtocolMessage response = result.getReplyMessage();
                        meshManager.renameNode(TEMP_NAME, response.getSource());
                        alreadyConnected.add(newConnection);
                        GossipTable gossipTable = (GossipTable) response.getProtocolParameter();
                        reloadTable(connections, alreadyConnected, gossipTable.getGossipTable());
                    } else if (replyCommand == obuzzName) {
                        String source = result.getReplyMessage().getSource();
                        requestProcessor.asyncBroadcast(byebye);
                        meshManager.close();
                        throw new ProtocolException("Offensive name for component: " + source);
                    }
                }
            }
            log(MeshProtocolManager.class, INFORM, "Connected to all the peers !");
        } else {
            log(MeshProtocolManager.class, INFORM, "Nobody to connect to. Sitting here ...");
        }
        negotiateHeader(remotePeer != null);
    }

    private void negotiateHeader(boolean broadcast) {
        boolean allAgreed = false;
        int tries = 0;

        byte[] headerKey = TopicEncodingKey.generateRandomHeader();

        while (!allAgreed && tries < MAX_TURNS) {
            allAgreed = true;
            tries++;

            headerKey = TopicEncodingKey.generateRandomHeader();
            Queue<String> queue = new LinkedList<>(meshManager.getConnectorIds());
            while (allAgreed && !queue.isEmpty()) {
                String target = queue.poll();

                ProcessingResult result = requestProcessor.syncRequest(negRequest, target, new HeaderKey(headerKey))
                                                          .addValidReply(negRequestAck).addValidReply(negRequestRej)
                                                          .setMaxRepeatsOnBusy(1).process(true);
                if (result instanceof FailResult) {
                    if (((FailResult) result).getFailReason() == FailResult.FailReason.busy) {
                        queue.offer(target);
                    } else {
                        String source = result.getReplyMessage().getSource();
                        log(MeshProtocolManager.class, INFORM, "Unknown reply from: " + source);
                    }
                } else {
                    ProtocolCommand replyCommand = result.getReplyMessage().getProtocolCommand();
                    String source = result.getReplyMessage().getSource();
                    if (replyCommand == negRequestAck) {
                        log(MeshProtocolManager.class, INFORM, "Ack on header key from: " + source);
                    } else if (replyCommand == negRequestRej) {
                        log(MeshProtocolManager.class, INFORM, "Reject the header key from: " + source);
                        allAgreed = false;
                    }
                }
            }
        }

        if (allAgreed) {
            TopicEncodingKey.init(headerKey);
            meshManager.setConnected(true);
            if (broadcast) {
                TcpAddress localAddress = new TcpAddress(networkManager.getLocalAddress(),
                                                         meshManager.getInboundPort());
                requestProcessor.asyncBroadcast(negResult, new TcpAddressParameter(localAddress));
            }
        } else {
            requestProcessor.asyncBroadcast(byebye);
            meshManager.close();
            throw new ProtocolException("Could not negotiate the header key in " + MAX_TURNS + " turns!");
        }
    }

    private void reloadTable(Queue<TcpAddress> connections, Set<TcpAddress> alreadyConnected,
                             List<TcpAddress> gossipTable) {
        if (gossipTable != null) {
            connections.addAll(gossipTable.stream().filter(e -> !alreadyConnected.contains(e)).collect(toList()));
            log(MeshProtocolManager.class, INFORM, "Reload gossip table, new connections: " + connections);
        }
    }

    private ProtocolMessage createNegotiateMessage(ProtocolMessage message) {
        HeaderKey key = (HeaderKey) message.getProtocolParameter();
        if (key == null) {
            log(MeshProtocolManager.class, INFORM, "Comparing keys: remote key is null");
            return protocolHandler.message(negRequestRej, message.getSource());
        } else {
            log(MeshProtocolManager.class, INFORM, "Comparing keys: remote=%s local=%s",
                key.getHeaderKey(), TopicEncodingKey.getHeader());
            if (!Arrays.equals(key.getHeaderKey(), TopicEncodingKey.getHeader())) {
                return protocolHandler.message(negRequestAck, message.getSource());
            } else {
                return protocolHandler.message(negRequestRej, message.getSource());
            }
        }
    }

    private ProtocolMessage createNegotiateResult(ProtocolMessage message) {
        log(MeshProtocolManager.class, INFORM, "Adding new mesh node: " + message);
        meshManager.connect(message.getSource(), ((TcpAddressParameter) message.getProtocolParameter()).getAddress());
        log(MeshProtocolManager.class, INFORM, "Received NEG RES sending ACK to: " + message.getSource());
        return protocolHandler.message(negResultAck, message.getSource());
    }

    private ProtocolMessage createSalutReply(ProtocolMessage message) {
        String sourceId = message.getSource();
        if (sourceId != null && !sourceId.equals(containerId)) {
            log(MeshProtocolManager.class, INFORM, "Received salut sending salut reply from: " + containerId);
            return protocolHandler.message(buzzEcho, sourceId, createGossipTable());
        } else {
            log(MeshProtocolManager.class, INFORM, "Received salut sending offensive from: " + containerId);
            return protocolHandler.message(obuzzName, sourceId);
        }
    }

    private GossipTable createGossipTable() {
        return new GossipTable().addAll(meshManager.getConnectors().values().stream().map(MeshConnector::getAddress)
                                                   .collect(toList()));
    }

    private ProtocolMessage loadContainer(ProtocolMessage message) {
        ProjectRelatedParameter param = (ProjectRelatedParameter) message.getProtocolParameter();
        if (param == null || param.getContainerConfig() == null ||
            param.getContainerConfig().getContainerId() == null ||
            !param.getContainerConfig().getContainerId().equals(containerId)) {
            RejectReason reason = new RejectReason(300, "Invalid parameter or container id provided!");
            return protocolHandler.message(loadContainerRej, message.getSource(), reason);
        }
        if (loadLocalContainer(containerId, param.getContainerConfig(), networkManager)) {
            return protocolHandler.message(loadContainerAck, message.getSource());
        } else {
            RejectReason reason = new RejectReason(300, "Could not load container ...");
            return protocolHandler.message(loadContainerRej, message.getSource(), reason);
        }
    }

    private ProtocolMessage componentManagerChanged(ProtocolMessage message) {
        ComponentManagerChangeEvent changeEvent = (ComponentManagerChangeEvent) message.getProtocolParameter();
        switch (changeEvent.getEventSource()) {
            case host:
                if (changeEvent.getType() == ChangeEvent.Type.add) {
                    componentManager.addHost(changeEvent.getComponentType(), changeEvent.getComponentId(), changeEvent.getTcpAddress());
                } else {
                    componentManager.removeHost(changeEvent.getComponentType(), changeEvent.getComponentId());
                }
                break;
            case encoding:
                if (changeEvent.getType() == ChangeEvent.Type.add) {
                    componentManager.addTopic(changeEvent.getComponentType(), changeEvent.getTopicKey(), changeEvent.getEncoding());
                } else {
                    componentManager.removeTopic(changeEvent.getComponentType(), changeEvent.getTopicKey(), changeEvent.getEncoding());
                }
                break;
            case component:
                if (changeEvent.getType() == ChangeEvent.Type.remove) {
                    componentManager.unsubscribeFrom(changeEvent.getComponentId());
                }

                if (changeEvent.getComponentType() == ComponentType.service) {
                    if (changeEvent.getType() == ChangeEvent.Type.add) {
                        componentManager.addService(changeEvent.getComponentName(), changeEvent.getComponentId());
                    } else {
                        componentManager.removeService(changeEvent.getComponentName(), changeEvent.getComponentId());
                    }
                }
                break;
        }
        return protocolHandler.message(componentManagerChangedAck, message.getSource());
    }

    @Override
    public void hostChanged(ChangeEvent changeEvent) {
        broadcastEvent(ComponentManagerChangeEvent.Source.host, changeEvent);
    }

    @Override
    public void encodingChanged(ChangeEvent changeEvent) {
        broadcastEvent(ComponentManagerChangeEvent.Source.encoding, changeEvent);
    }

    @Override
    public void componentChanged(ChangeEvent changeEvent) {
        broadcastEvent(ComponentManagerChangeEvent.Source.component, changeEvent);
    }

    private void broadcastEvent(ComponentManagerChangeEvent.Source source, ChangeEvent changeEvent) {
        if (containerId.equals(changeEvent.getContainerId())) {
            ComponentManagerChangeEvent e = new ComponentManagerChangeEvent(source, changeEvent);
            requestProcessor.asyncBroadcast(componentManagerChanged, e);
        }
    }
}
