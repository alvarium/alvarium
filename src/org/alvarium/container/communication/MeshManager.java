/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.communication;

import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.RemoteConnector;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.*;

public class MeshManager implements RemoteConnector {
    private MeshReceiver receiver;
    private final Map<String, MeshConnector> connectors = new HashMap<>();
    private MeshProtocolManager meshProtocolManager;
    private TerminalProtocolManager terminalProtocolManager;
    private boolean connected = false;
    private boolean closing = false;

    public MeshManager(String containerId, int port, NetworkManager networkManager) {
        meshProtocolManager = new MeshProtocolManager(containerId, this, networkManager);
        terminalProtocolManager = new TerminalProtocolManager(containerId, this, networkManager);

        receiver = new MeshReceiver(containerId, port, 2);
        receiver.start();
        receiver.addProtocolHandler(meshProtocolManager.getProtocolHandler());
        receiver.addProtocolHandler(terminalProtocolManager.getProtocolHandler());
    }

    public void initialise(TcpAddress remoteAddress) {
        meshProtocolManager.initialiseProtocol(remoteAddress);
    }

    public MeshConnector connect(String targetContainerId, TcpAddress address) {
        synchronized (connectors) {
            if (connectors.containsKey(targetContainerId)) {
                return connectors.get(targetContainerId);
            } else {
                MeshConnector result = new MeshConnector(targetContainerId, address);
                result.open();
                connectors.put(targetContainerId, result);
                return result;
            }
        }
    }

    public void remove(String containerId) {
        synchronized (connectors) {
            MeshConnector connector = connectors.remove(containerId);
            if (connector != null) {
                connector.close();
            }
        }
    }

    public void renameNode(String oldName, String newName) {
        synchronized (connectors) {
            MeshConnector connector = connectors.remove(oldName);
            if (connector != null) {
                connector.setContainerId(newName);
                connectors.put(newName, connector);
            }
        }
    }

    public int getInboundPort() {
        return receiver.getPort();
    }

    public Map<String, MeshConnector> getConnectors() {
        return connectors;
    }

    public Set<String> getConnectorIds() {
        return Collections.unmodifiableSet(connectors.keySet());
    }

    private void broadcast(ProtocolMessage protocolMessage) {
        synchronized (connectors) {
            for (MeshConnector connector : connectors.values()) {
                connector.request(protocolMessage);
            }
        }
    }

    private ProtocolMessage requestFrom(String targetId, ProtocolMessage protocolMessage) {
        MeshConnector connector;
        synchronized (connectors) {
            connector = connectors.get(targetId);
        }
        if (connector != null) {
            return connector.request(protocolMessage);
        } else {
            return invalidMessage;
        }
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
        meshProtocolManager.getProtocolHandler().setInitialised(connected);
        terminalProtocolManager.getProtocolHandler().setInitialised(connected);
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public ProtocolMessage request(ProtocolMessage protocolMessage) {
        String targetId = protocolMessage.getTarget();
        if (targetId == null) {
            return invalidMessage;
        } else if (targetId.equals(ANY)) {
            broadcast(protocolMessage);
            return broadcastResult;
        } else {
            return requestFrom(targetId, protocolMessage);
        }
    }

    @Override
    public Set<String> getRemoteContainerIds() {
        return Collections.unmodifiableSet(connectors.keySet());
    }

    @Override
    public long getLastPing(String remoteId) {
        MeshConnector connector;
        synchronized (connectors) {
            connector = connectors.get(remoteId);
        }
        if (connector != null) {
            return connector.getLastPing();
        } else {
            return -1;
        }
    }

    public TerminalProtocolManager getTerminalProtocolManager() {
        return terminalProtocolManager;
    }

    public void close() {
        if (!closing) {
            closing = true;

            log(MeshManager.class, INFORM, "Closing the mesh manager. Removing the protocol handlers ...");
            receiver.removeProtocolHandler(meshProtocolManager.getProtocolHandler());
            receiver.removeProtocolHandler(terminalProtocolManager.getProtocolHandler());

            log(MeshManager.class, INFORM, "Closing the mesh manager. Closing the receiver ...");
            receiver.close();
            log(MeshManager.class, INFORM, "Closing the mesh manager. Closing the mesh connectors ...");
            synchronized (connectors) {
                connectors.values().forEach(MeshConnector::close);
            }

            log(MeshManager.class, INFORM, "Closing the mesh manager. Mesh close ...");
            meshProtocolManager.close();
            log(MeshManager.class, INFORM, "Closing the mesh manager. Terminal manager close ...");
            terminalProtocolManager.close();
        }
    }
}
