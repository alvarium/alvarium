/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.communication;

import org.alvarium.component.base.Closeable;
import org.alvarium.container.model.ContainerConfig;
import org.alvarium.container.model.ProjectConfig;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.handler.ProtocolHandler;
import org.alvarium.protocol.parameters.GossipTable;
import org.alvarium.protocol.parameters.ProjectRelatedParameter;
import org.alvarium.protocol.parameters.RejectReason;
import org.alvarium.protocol.processor.RequestProcessor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static org.alvarium.container.ContainerUtils.closeLocalContainer;
import static org.alvarium.container.ContainerUtils.loadLocalContainer;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.ProtocolRole.admin;
import static org.alvarium.protocol.ProtocolRole.mesh;
import static org.alvarium.protocol.command.AdminProtocolCommands.*;
import static org.alvarium.protocol.command.GenericProtocolCommands.*;
import static org.alvarium.protocol.command.MeshProtocolCommands.*;

public class TerminalProtocolManager implements Closeable {
    private String containerId;
    private MeshManager meshManager;
    private NetworkManager networkManager;

    private ProtocolHandler protocolHandler;
    private RequestProcessor requestProcessor;

    public TerminalProtocolManager(String containerId, MeshManager meshManager, NetworkManager networkManager) {
        this.containerId = containerId;
        this.meshManager = meshManager;
        this.networkManager = networkManager;

        initHandler();
        initRequester();
    }

    private void initHandler() {
        protocolHandler = new ProtocolHandler(containerId, admin);
        protocolHandler.registerDefault(m -> {
            log(MeshProtocolManager.class, INFORM, "Received nothing useful from" + m.getSource() + " ,sending rtfm!");
            return protocolHandler.message(obuzz_rtfm, ANY);
        });
        protocolHandler.register(buzz, m -> {
            log(TerminalProtocolManager.class, INFORM, "Received salut from: " + m.getSource());
            return protocolHandler.message(buzzEcho, m.getSource(), createGossipTable());
        });
        protocolHandler.register(byebye, m -> {
            log(TerminalProtocolManager.class, INFORM, "Received Bye-bye from: " + m.getSource());
            return protocolHandler.message(farewell, m.getSource());
        });
        protocolHandler.register(hey, m -> {
            log(MeshProtocolManager.class, INFORM, "Received hey from: " + m.getSource());
            return protocolHandler.message(ho, m.getSource());
        });

        protocolHandler.register(adminLoadProject, this::loadProject);
    }

    private void initRequester() {
        requestProcessor = new RequestProcessor(containerId, mesh, meshManager).addBusyReply(buzzy);
    }

    public String getContainerId() {
        return containerId;
    }

    public ProtocolHandler getProtocolHandler() {
        return protocolHandler;
    }

    private GossipTable createGossipTable() {
        GossipTable result = new GossipTable();
        result.getGossipTable().addAll(meshManager.getConnectors().values().stream()
                                                  .map(MeshConnector::getAddress).collect(toList()));
        return result;
    }

    private ProtocolMessage loadProject(ProtocolMessage message) {
        if (message.getProtocolParameter() != null) {
            ProjectRelatedParameter parameter = (ProjectRelatedParameter) message.getProtocolParameter();
            ProjectConfig projectConfig = parameter.getProjectConfig();

            String invalidContainerId = validateContainerIds(projectConfig.getContainerIds());
            if (invalidContainerId != null) {
                RejectReason reason = new RejectReason(101, "Unknown container id provided: " + invalidContainerId);
                return protocolHandler.message(adminLoadProjectRej, message.getSource(), reason);
            }

            Iterator<String> projectIterator = projectConfig.getContainerIds().iterator();
            boolean error = false;
            while (!error && projectIterator.hasNext()) {
                String projectContainerId = projectIterator.next();
                if (containerId.equals(projectContainerId)) {
                    if (!loadLocalContainer(containerId, projectConfig.getConfig(containerId), networkManager)) {
                        error = true;
                        invalidContainerId = projectContainerId;
                    }
                } else {
                    if (!loadRemoteContainer(projectContainerId, projectConfig.getConfig(projectContainerId))) {
                        error = true;
                        invalidContainerId = projectContainerId;
                    }
                }
            }

            if (error) {
                closeLocalContainer(containerId);
                requestProcessor.asyncBroadcast(rollbackContainer);
                RejectReason rejectReason = new RejectReason(101, "Could not load container: " + invalidContainerId);
                return protocolHandler.message(adminLoadProjectRej, message.getSource(), rejectReason);
            } else {
                return protocolHandler.message(adminLoadProjectAck, message.getSource());
            }
        } else {
            RejectReason rejectReason = new RejectReason(100, "Invalid protocol parameter!");
            return protocolHandler.message(adminLoadProjectRej, message.getSource(), rejectReason);
        }
    }

    private boolean loadRemoteContainer(String containerId, ContainerConfig containerConfig) {
        return requestProcessor.syncRequest(loadContainer, containerId, new ProjectRelatedParameter(containerConfig))
                               .setMaxRepeatsOnBusy(10).addValidReply(loadContainerAck).process().isValid();
    }

    private String validateContainerIds(Set<String> projectContainerIds) {
        Set<String> meshContainerIds = new HashSet<>(meshManager.getConnectorIds());
        meshContainerIds.add(containerId);
        for (String containerId : projectContainerIds) {
            if (!meshContainerIds.contains(containerId)) {
                return containerId;
            }
        }
        return null;
    }

    @Override
    public void close() {
        if (requestProcessor != null) {
            requestProcessor.close();
        }
    }
}
