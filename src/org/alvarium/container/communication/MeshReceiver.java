/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.communication;

import org.alvarium.component.data.InvalidDataException;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.handler.ProtocolHandler;
import org.zeromq.ZMQ;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.MessageFactory.messageFactory;
import static org.alvarium.protocol.ProtocolRole.unknown;
import static org.alvarium.protocol.command.GenericProtocolCommands.obuzz_rtfm;

public class MeshReceiver extends Thread {
    private ZMQ.Context context;
    private ZMQ.Socket sender;
    private boolean running = true;
    private String containerId;
    private int port;

    private final List<ProtocolHandler> protocolHandlers = new LinkedList<>();

    public MeshReceiver(String containerId, int port, int ioThreads) {
        this.containerId = containerId;
        this.port = port;

        context = ZMQ.context(ioThreads);
        sender = context.socket(ZMQ.REP);
        sender.bind("tcp://*:" + port);
    }

    public void run() {
        while (running) {
            byte[] data = sender.recv(0);
            if (data != null) {
                try {
                    data = convertData(handleRequest(convertData(data)));
                    sender.send(data, 0);
                } catch (InvalidDataException e) {
                    log(MeshReceiver.class, CRITICAL, "Invalid data on container = " + containerId, e);
                }
            }
        }
        sender.close();
        context.term();
    }

    public void addProtocolHandler(ProtocolHandler protocolHandler) {
        protocolHandlers.add(protocolHandler);
    }

    public void removeProtocolHandler(ProtocolHandler protocolHandler) {
        protocolHandlers.add(protocolHandler);
    }

    protected ProtocolMessage handleRequest(ProtocolMessage message) {
        Optional<ProtocolHandler> selected =
                protocolHandlers.stream().filter(h -> h.isMessageSupported(message)).findFirst();
        if (selected.isPresent()) {
            return selected.get().handle(message);
        } else {
            return new ProtocolMessage(obuzz_rtfm, containerId, message.getSource(), unknown);
        }
    }

    protected byte[] convertData(ProtocolMessage data) throws InvalidDataException {
        return messageFactory.writeObject(data);
    }

    protected ProtocolMessage convertData(byte[] data) throws InvalidDataException {
        log(MeshReceiver.class, DEBUG, "Convert data=%s", Arrays.toString(data));
        return messageFactory.readObject(data);
    }

    public int getPort() {
        return port;
    }

    public void close() {
        running = false;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            //--
        }
    }
}
