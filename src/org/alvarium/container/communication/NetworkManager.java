/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.communication;

import org.alvarium.container.datatypes.TcpAddress;

import java.io.IOException;
import java.net.*;
import java.util.Enumeration;
import java.util.Random;

public class NetworkManager {
    public static final int MAX_ATTEMPTS = 10;
    private static final Random rnd = new Random(System.currentTimeMillis());

    private String localAddress = "127.0.0.1";

    private Integer startPort = null;
    private Integer endPort = null;

    public NetworkManager() {
        resolveLocalHost();
    }

    public NetworkManager(Integer startPort, Integer endPort) {
        this();
        this.startPort = startPort;
        this.endPort = endPort;
    }

    private void resolveLocalHost() {
        try {
            Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
            while (nis.hasMoreElements()) {
                NetworkInterface ni = nis.nextElement();
                Enumeration<InetAddress> ias = ni.getInetAddresses();
                while (ias.hasMoreElements()) {
                    InetAddress ia = ias.nextElement();
                    if (!ia.isLinkLocalAddress() && !ia.isLoopbackAddress() && ia instanceof Inet4Address) {
                        localAddress = ia.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public int findRandomFreePort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        } catch (IOException e) {
            //-- ignore
        }
        return -1;
    }

    public int findRandomFreePort(int start, int end) {
        for (int i = 0; i < MAX_ATTEMPTS; i++) {
            int port = rnd.nextInt(end - start) + start;
            try (ServerSocket socket = new ServerSocket(port)) {
                return socket.getLocalPort();
            } catch (IOException e) {
                //-- ignore
            }
        }
        return -1;
    }

    public int findFreePort(int start, int end) {
        for (int port = start; port <= end; port++) {
            try (ServerSocket socket = new ServerSocket(port)) {
                return socket.getLocalPort();
            } catch (IOException e) {
                //-- ignore
            }
        }
        return -1;
    }

    public TcpAddress findRandomFreeAddress() {
        int port;
        if (startPort != null && endPort != null) {
            port = findRandomFreePort(startPort, endPort);
        } else {
            port = findRandomFreePort();
        }

        if (port > 0) {
            return new TcpAddress(localAddress, port);
        } else {
            return null;
        }
    }

    public static boolean isPortAvailable(int port) {
        try (ServerSocket socket = new ServerSocket(port)) {
            return socket.isBound();
        } catch (IOException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        NetworkManager manager = new NetworkManager();
        System.out.println(manager.localAddress);
        for (int i = 0; i < 10; i++) {
            System.out.println(manager.findRandomFreeAddress());
        }
    }
}
