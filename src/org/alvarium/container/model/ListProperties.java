/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import java.util.*;

public class ListProperties {
    public final static ListProperties empty = new ListProperties() {
        public void addProperty(String name, String value) {
            //-- removed
        }
    };

    private Map<String, List<String>> listProperties = new HashMap<>();

    public ListProperties() {
    }

    public ListProperties(ListProperties properties) {
        addAll(properties);
    }

    public void addProperty(String name, String value) {
        List<String> values = listProperties.get(name);
        if (values == null) {
            values = new LinkedList<>();
            listProperties.put(name, values);
        }
        values.add(value);
    }

    public void addProperty(String name, List<String> values) {
        List<String> valueList = listProperties.get(name);
        if (valueList == null) {
            valueList = new LinkedList<>();
            listProperties.put(name, valueList);
        }
        valueList.addAll(values);
    }

    public void addAll(ListProperties properties) {
        for (Map.Entry<String, List<String>> item : properties.listProperties.entrySet()) {
            addProperty(item.getKey(), item.getValue());
        }
    }

    public Map<String, List<String>> getListProperties() {
        return listProperties;
    }

    public void removeProperty(String name) {
        listProperties.remove(name);
    }

    public Set<String> getPropertyTags() {
        return Collections.unmodifiableSet(listProperties.keySet());
    }

    public boolean hasProperty(String name) {
        return listProperties.containsKey(name);
    }

    public String getProperty(String name) {
        if (listProperties.containsKey(name)) {
            return listProperties.get(name).iterator().next();
        } else {
            return null;
        }
    }

    public String getProperty(String name, String defaultProperty) {
        if (hasProperty(name)) {
            return getProperty(name);
        } else {
            return defaultProperty;
        }
    }

    public List<String> getPropertyList(String name) {
        if (listProperties.containsKey(name)) {
            return Collections.unmodifiableList(listProperties.get(name));
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public String toString() {
        return listProperties.toString();
    }
}
