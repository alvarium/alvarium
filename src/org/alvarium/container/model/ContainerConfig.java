/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import java.util.LinkedList;
import java.util.List;

public class ContainerConfig {
    private String containerId;
    private List<ComponentConfig> componentConfigs = new LinkedList<>();
    private List<SchedulerConfig> schedulerConfigs = new LinkedList<>();
    private List<ServiceConfig> serviceConfigs = new LinkedList<>();

    public ContainerConfig() {
    }

    public ContainerConfig(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public List<ComponentConfig> getComponentConfigs() {
        return componentConfigs;
    }

    public void addComponentConfig(ComponentConfig componentConfig) {
        this.componentConfigs.add(componentConfig);
    }

    public List<SchedulerConfig> getSchedulerConfigs() {
        return schedulerConfigs;
    }

    public void addSchedulerConfig(SchedulerConfig schedulerConfig) {
        this.schedulerConfigs.add(schedulerConfig);
    }

    public List<ServiceConfig> getServiceConfigs() {
        return serviceConfigs;
    }

    public void addServiceConfig(ServiceConfig serviceConfig) {
        this.serviceConfigs.add(serviceConfig);
    }

    @Override
    public String toString() {
        return "ContainerConfig{" +
                "containerId='" + containerId + '\'' +
                ", componentConfigs=" + componentConfigs +
                ", schedulerConfigs=" + schedulerConfigs +
                ", serviceConfigs=" + serviceConfigs +
                '}';
    }
}
