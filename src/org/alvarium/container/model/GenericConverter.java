/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.xml.*;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.alvarium.container.manager.ComponentManager.componentManager;

public class GenericConverter {
    public static final GenericConverter genericConverter = new GenericConverter();

    public GenericConverter() {
    }

    public ProjectConfig convert(ProjectXmlConfig projectXmlConfig, boolean resolveComponentAddresses) {
        ProjectConfig result = new ProjectConfig();
        for (ContainerXmlConfig containerXmlConfig : projectXmlConfig.getContainers()) {
            result.addConfig(convert(containerXmlConfig, resolveComponentAddresses));
        }
        return result;
    }

    public ContainerConfig convert(ContainerXmlConfig containerXmlConfig, boolean resolveComponentAddresses) {
        String containerId = containerXmlConfig.getContainerId();
        ContainerConfig result = new ContainerConfig(containerId);

        for (SchedulerXmlConfig xmlConfig : containerXmlConfig.getSchedulerConfigs()) {
            result.addSchedulerConfig(convert(containerId, xmlConfig, resolveComponentAddresses));
        }

        for (ServiceXmlConfig xmlConfig : containerXmlConfig.getServiceConfigs()) {
            result.addServiceConfig(convert(containerId, xmlConfig, resolveComponentAddresses));
        }

        for (ComponentXmlConfig xmlConfig : containerXmlConfig.getComponentConfigs()) {
            result.addComponentConfig(convert(containerId, xmlConfig, resolveComponentAddresses));
        }

        return result;
    }

    public SchedulerConfig convert(String containerId, SchedulerXmlConfig schedulerXmlConfig, boolean resolveAddress) {
        ComponentId componentId = new ComponentId(containerId, schedulerXmlConfig.getId());
        TcpAddress tcpAddress = null;
        if (resolveAddress) {
            tcpAddress = componentManager.resolve(componentId);
        }
        return new SchedulerConfig(componentId, tcpAddress, schedulerXmlConfig.getTimeout());
    }

    public ServiceConfig convert(String containerId, ServiceXmlConfig xmlConfig, boolean resolveAddress) {
        ComponentId componentId = new ComponentId(containerId, xmlConfig.getId());
        TcpAddress tcpAddress = null;
        if (resolveAddress) {
            tcpAddress = componentManager.resolve(componentId);
        }
        ServiceConfig result = new ServiceConfig(componentId, tcpAddress, xmlConfig.getName(),
                                                 xmlConfig.getServiceClass(), xmlConfig.getIoThreads());
        result.setPropertyConfigs(convert(xmlConfig.getPropertyConfigs()));
        return result;
    }

    public ComponentConfig convert(String containerId, ComponentXmlConfig componentXmlConfig, boolean resolveAddress) {
        ComponentConfig config = new ComponentConfig();

        ComponentId componentId = new ComponentId(containerId, componentXmlConfig.getId());
        config.setComponentId(componentId);
        config.setComponentClass(componentXmlConfig.getComponentClass());
        if (resolveAddress) {
            config.setAddress(componentManager.resolve(config.getComponentId()));
        } else {
            config.setAddress(null);
        }
        config.setSchedulerId(convert(componentXmlConfig.getScheduler()));
        config.getPublishConfigs().addAll(componentXmlConfig.getPublishConfigs().stream().map(GenericConverter::convert).collect(toList()));
        config.getSubscriptionConfigs().addAll(componentXmlConfig.getSubscriptionConfigs().stream().map(GenericConverter::convert).collect(toList()));
        config.setPropertyConfigs(convert(componentXmlConfig.getPropertyConfigs()));
        return config;
    }

    public static ComponentId convert(SchedulerPropertyXmlConfig schedulerXmlConfig) {
        if (schedulerXmlConfig == null) {
            return null;
        } else {
            return new ComponentId(schedulerXmlConfig.getContainer(), schedulerXmlConfig.getId());
        }
    }

    public static PublishConfig convert(PublishXmlConfig xmlConfig) {
        return new PublishConfig(xmlConfig.getTopic(), xmlConfig.getInternalTopic());
    }

    public static SubscriptionConfig convert(SubscriptionXmlConfig xmlConfig) {
        ComponentId componentId = new ComponentId(xmlConfig.getContainerId(), xmlConfig.getComponentId());
        return new SubscriptionConfig(xmlConfig.getTopic(), componentId);
    }

    public static ListProperties convert(List<PropertyXmlConfig> list) {
        ListProperties result = new ListProperties();
        for (PropertyXmlConfig item : list) {
            result.addProperty(item.getProperty(), item.getValue());
        }
        return result;
    }
}
