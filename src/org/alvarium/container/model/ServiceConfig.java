/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;

public class ServiceConfig {
    private ComponentId componentId;
    private TcpAddress tcpAddress;
    private String name;
    private String serviceClass;
    private Integer ioThreads;

    private ListProperties properties = new ListProperties();

    public ServiceConfig() {
    }

    public ServiceConfig(ComponentId componentId, TcpAddress tcpAddress, String name, String serviceClass, Integer ioThreads) {
        this.componentId = componentId;
        this.tcpAddress = tcpAddress;
        this.name = name;
        this.serviceClass = serviceClass;
        this.ioThreads = ioThreads;
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public void setComponentId(ComponentId componentId) {
        this.componentId = componentId;
    }

    public TcpAddress getAddress() {
        return tcpAddress;
    }

    public void setAddress(TcpAddress tcpAddress) {
        this.tcpAddress = tcpAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIoThreads() {
        return ioThreads;
    }

    public void setIoThreads(Integer ioThreads) {
        this.ioThreads = ioThreads;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    public ListProperties getPropertyConfigs() {
        return properties;
    }

    public void setPropertyConfigs(ListProperties propertyConfigs) {
        this.properties = propertyConfigs;
    }

    @Override
    public String toString() {
        return "ServiceConfig{" +
               "componentId=" + componentId +
               ", tcpAddress=" + tcpAddress +
               ", name='" + name + '\'' +
               ", serviceClass='" + serviceClass + '\'' +
               ", ioThreads=" + ioThreads +
               ", properties=" + properties +
               '}';
    }
}
