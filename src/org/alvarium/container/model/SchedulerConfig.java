/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;

public class SchedulerConfig {
    private ComponentId componentId;
    private TcpAddress address;
    private int timeout;

    public SchedulerConfig() {
    }

    public SchedulerConfig(ComponentId componentId, TcpAddress address, int timeout) {
        this.componentId = componentId;
        this.address = address;
        this.timeout = timeout;
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public void setComponentId(ComponentId componentId) {
        this.componentId = componentId;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public TcpAddress getAddress() {
        return address;
    }

    public void setAddress(TcpAddress address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "SchedulerConfig{" +
               "componentId=" + componentId +
               ", address=" + address +
               ", timeout=" + timeout +
               '}';
    }
}
