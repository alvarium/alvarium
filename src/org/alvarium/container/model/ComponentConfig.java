/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.model;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;

import java.util.LinkedList;
import java.util.List;

public class ComponentConfig {
    private ComponentId componentId;
    private String componentClass; //Class<AbstractComponent>
    private TcpAddress address;
    private ComponentId schedulerId;
    private List<SubscriptionConfig> subscriptionConfigs = new LinkedList<>();
    private List<PublishConfig> publishConfigs = new LinkedList<>();
    private ListProperties propertyConfigs = new ListProperties();

    public ComponentConfig() {
    }

    public ComponentConfig(ComponentId componentId, String componentClass,
                           TcpAddress address, ComponentId schedulerId,
                           List<SubscriptionConfig> subscriptionConfigs,
                           List<PublishConfig> publishConfigs,
                           ListProperties propertyConfigs) {
        this.componentId = componentId;
        this.componentClass = componentClass;
        this.address = address;
        this.schedulerId = schedulerId;
        this.subscriptionConfigs = subscriptionConfigs;
        this.publishConfigs = publishConfigs;
        this.propertyConfigs = propertyConfigs;
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public void setComponentId(ComponentId componentId) {
        this.componentId = componentId;
    }

    public String getComponentClass() {
        return componentClass;
    }

    public void setComponentClass(String componentClass) {
        this.componentClass = componentClass;
    }

    public TcpAddress getAddress() {
        return address;
    }

    public void setAddress(TcpAddress address) {
        this.address = address;
    }

    public ComponentId getSchedulerId() {
        return schedulerId;
    }

    public void setSchedulerId(ComponentId schedulerId) {
        this.schedulerId = schedulerId;
    }

    public List<SubscriptionConfig> getSubscriptionConfigs() {
        return subscriptionConfigs;
    }

    public List<PublishConfig> getPublishConfigs() {
        return publishConfigs;
    }

    public ListProperties getPropertyConfigs() {
        return propertyConfigs;
    }

    public void setPropertyConfigs(ListProperties propertyConfigs) {
        this.propertyConfigs = propertyConfigs;
    }

    @Override
    public String toString() {
        return "ComponentConfig{" +
               "componentId=" + componentId +
               ", componentClass=" + componentClass +
               ", address=" + address +
               ", schedulerId=" + schedulerId +
               ", subscriptionConfigs=" + subscriptionConfigs +
               ", publishConfigs=" + publishConfigs +
               ", propertyConfigs=" + propertyConfigs +
               '}';
    }
}
