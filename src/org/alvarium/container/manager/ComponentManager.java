/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.manager;

import org.alvarium.component.base.AbstractComponent;
import org.alvarium.component.base.Closeable;
import org.alvarium.component.service.Scheduler;
import org.alvarium.component.service.Service;
import org.alvarium.component.service.ServiceClient;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.datatypes.TopicKey;
import org.alvarium.utils.UVCache;

import java.util.HashMap;
import java.util.Map;

import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

public class ComponentManager {
    public static final ComponentManager componentManager = new ComponentManager();

    private final Map<ComponentId, TcpAddress> componentHostCache = new HashMap<>();
    private final UVCache<TopicKey, TopicEncodingKey> topicCache = new UVCache<>();

    private final Map<ComponentId, ComponentWrapper> componentMap = new HashMap<>();

    private EventManager eventManager;
    private ServiceManager serviceManager;

    public final SubscribeQueueManager queueManager;

    private ComponentManager() {
        eventManager = new EventManager();
        serviceManager = new ServiceManager();

        queueManager = new SubscribeQueueManager();
        eventManager.addListener(queueManager);
    }

    public void addListener(ManagerChangeListener listener) {
        eventManager.addListener(listener);
    }

    public void removeListener(ManagerChangeListener listener) {
        eventManager.removeListener(listener);
    }

    public void addScheduler(Scheduler scheduler) {
        ComponentId componentId = scheduler.getComponentId();
        addComponent(componentId, componentId.toString(), ComponentType.scheduler, scheduler);
    }

    public void addComponent(AbstractComponent component) {
        ComponentId componentId = component.getComponentId();
        addComponent(componentId, componentId.toString(), ComponentType.component, component);
    }

    public void addService(Service service) {
        addComponent(service.getComponentId(), service.getServiceName(), ComponentType.service, service);
        addService(service.getServiceName(), service.getComponentId());
    }

    private void addComponent(ComponentId componentId, String componentName, ComponentType componentType,
                              Closeable component) {
        synchronized (componentMap) {
            componentMap.put(componentId, new ComponentWrapper(component, componentName));
        }
        eventManager.fireComponentChange(ChangeEvent.Type.add, componentType, componentName, componentId);
    }

    public void removeComponent(ComponentId componentId) {
        boolean fireEvent = false;
        ComponentType componentType = ComponentType.unknown;
        String componentName = null;
        synchronized (componentMap) {
            ComponentWrapper componentWrapper = componentMap.remove(componentId);
            if (componentWrapper != null) {
                componentWrapper.close();
                unsubscribeFrom(componentId);
                fireEvent = true;
                componentType = componentWrapper.getType();
                componentName = componentWrapper.getComponentName();
            }
        }
        if (componentName != null) {
            removeHost(componentType, componentId);
        }

        if (fireEvent) {
            if (componentType == ComponentType.service) {
                removeService(componentName, componentId);
            }
            eventManager.fireComponentChange(ChangeEvent.Type.remove, componentType, componentName, componentId);
        }
    }

    public void unsubscribeFrom(ComponentId componentId) {
        if (componentId != null) {
            synchronized (componentMap) {
                componentMap.values().stream().filter(cw -> cw.getType() == ComponentType.component)
                            .forEach(componentWrapper -> {
                                AbstractComponent component = componentWrapper.getComponent();
                                component.unsubscribe(componentId);
                            });
            }
        }
    }

    public <T> T getComponent(ComponentId componentId) {
        synchronized (componentMap) {
            if (componentMap.containsKey(componentId)) {
                return componentMap.get(componentId).getComponent();
            } else {
                return null;
            }
        }
    }

    public void addService(String serviceName, ComponentId service) {
        serviceManager.addService(serviceName, service);
    }

    public void removeService(String serviceName, ComponentId service) {
        serviceManager.removeService(serviceName, service);
    }

    public ServiceClient getClient(String serviceName) {
        return serviceManager.getClient(serviceName);
    }

    public void closeAllComponents() {
        synchronized (componentMap) {
            synchronized (topicCache) {
                synchronized (componentHostCache) {
                    log(ComponentManager.class, INFORM, "Closing all components. Closing components: "
                            + componentMap.values());
                    componentMap.values().forEach(Closeable::close);
                    log(ComponentManager.class, INFORM, "Closing all components. Cleanup ...");
                    componentMap.clear();
                    topicCache.removeAll();
                    componentHostCache.clear();
                }
            }
        }
    }

    public ComponentType getComponentType(ComponentId componentId) {
        synchronized (componentMap) {
            ComponentWrapper wrapper = componentMap.get(componentId);
            if (wrapper == null) {
                return ComponentType.unknown;
            } else {
                return wrapper.getType();
            }
        }
    }

    public boolean addTopic(String topic, ComponentId componentId) {
        synchronized (topicCache) {
            TopicKey topicKey = new TopicKey(topic, componentId);
            if (!topicCache.containsU(topicKey)) {
                TopicEncodingKey encoding = TopicEncodingKey.createNewEncodingKey();

                topicCache.put(topicKey, encoding);
                ComponentType componentType = getComponentType(componentId);
                eventManager.fireEncodingChange(ChangeEvent.Type.add, componentType, topicKey, encoding);
            }
        }
        return true;
    }

    public boolean addTopic(ComponentType componentType, TopicKey topicKey, TopicEncodingKey encoding) {
        if (encoding != null && topicKey != null) {
            topicCache.put(topicKey, encoding);
            eventManager.fireEncodingChange(ChangeEvent.Type.add, componentType, topicKey, encoding);
        }
        return true;
    }

    public boolean removeTopic(String topic, ComponentId componentId) {
        TopicKey topicKey = new TopicKey(topic, componentId);
        TopicEncodingKey encoding = topicCache.removeU(topicKey);
        eventManager.fireEncodingChange(ChangeEvent.Type.remove, getComponentType(componentId), topicKey, encoding);
        return true;
    }

    public boolean removeTopic(ComponentType componentType, TopicKey topicKey, TopicEncodingKey encoding) {
        if (encoding != null && topicKey != null) {
            topicCache.removeU(topicKey);
            eventManager.fireEncodingChange(ChangeEvent.Type.remove, componentType, topicKey, encoding);
        }
        return true;
    }

    public TopicKey getTopic(byte[] encoding) {
        synchronized (topicCache) {
            return topicCache.getU(new TopicEncodingKey(encoding));
        }
    }

    public byte[] getEncoding(String topic, ComponentId componentId) {
        synchronized (topicCache) {
            TopicKey topicKey = new TopicKey(topic, componentId);
            TopicEncodingKey encoding = topicCache.getV(topicKey);
            if (encoding != null) {
                return encoding.getKey();
            } else {
                System.err.println("Requested non existing topic: " + topic);
                return null;
            }
        }
    }

    public boolean addHost(ComponentId componentId, TcpAddress tcpAddress) {
        return addHost(getComponentType(componentId), componentId, tcpAddress);
    }

    public boolean addHost(ComponentType componentType, ComponentId componentId, TcpAddress tcpAddress) {
        if (componentId != null && tcpAddress != null) {
            synchronized (componentHostCache) {
                componentHostCache.put(componentId, tcpAddress);
                eventManager.fireHostChange(ChangeEvent.Type.add, componentType, componentId, tcpAddress);
            }
        }
        return true;
    }

    public boolean removeHost(ComponentType componentType, ComponentId componentId) {
        if (componentId != null) {
            synchronized (componentHostCache) {
                TcpAddress tcpAddress = componentHostCache.remove(componentId);
                if (tcpAddress != null) {
                    eventManager.fireHostChange(ChangeEvent.Type.remove, componentType, componentId, tcpAddress);
                }
            }
        }
        return true;
    }

    public TcpAddress resolve(ComponentId componentId) {
        synchronized (componentHostCache) {
            return componentHostCache.get(componentId);
        }
    }

    public boolean isResolvable(ComponentId componentId, String topic) {
        TcpAddress address = resolve(componentId);
        if (address == null) {
            return false;
        } else {
            byte[] encoding = getEncoding(topic, componentId);
            return encoding != null;
        }
    }

    public boolean isResolvable(ComponentId componentId) {
        return resolve(componentId) != null;
    }
}
