/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.manager;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.datatypes.TopicKey;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventManager {
    private final List<ManagerChangeListener> managerChangeListeners = new LinkedList<>();
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    public void addListener(ManagerChangeListener listener) {
        synchronized (managerChangeListeners) {
            managerChangeListeners.add(listener);
        }
    }

    public void removeListener(ManagerChangeListener listener) {
        synchronized (managerChangeListeners) {
            managerChangeListeners.remove(listener);
        }
    }

    public void fireHostChange(ChangeEvent.Type eventType, ComponentType componentType, ComponentId cId,
                               TcpAddress tcpAddress) {
        synchronized (managerChangeListeners) {
            final ChangeEvent event = new ChangeEvent(eventType, componentType, cId, tcpAddress);
            for (ManagerChangeListener listener : managerChangeListeners) {
                executorService.execute(() -> listener.hostChanged(event));
            }
        }
    }

    public void fireEncodingChange(ChangeEvent.Type eventType, ComponentType componentType,
                                   TopicKey topicKey, TopicEncodingKey topicEncodingKey) {
        synchronized (managerChangeListeners) {
            final ChangeEvent event = new ChangeEvent(eventType, componentType, topicKey, topicEncodingKey);
            for (ManagerChangeListener listener : managerChangeListeners) {
                executorService.execute(() -> listener.encodingChanged(event));
            }
        }
    }

    public void fireComponentChange(ChangeEvent.Type eventType, ComponentType componentType, String componentName,
                                    ComponentId componentId) {
        synchronized (managerChangeListeners) {
            final ChangeEvent event = new ChangeEvent(eventType, componentType, componentName, componentId);
            for (ManagerChangeListener listener : managerChangeListeners) {
                executorService.execute(() -> listener.componentChanged(event));
            }
        }
    }
}
