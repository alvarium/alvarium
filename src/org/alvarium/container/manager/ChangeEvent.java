/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.manager;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.datatypes.TopicEncodingKey;
import org.alvarium.container.datatypes.TopicKey;

public class ChangeEvent {
    public enum Type {
        add,
        remove
    }

    private Type type;
    private String componentName;
    private String containerId;
    private ComponentId componentId;
    private ComponentType componentType;
    private TcpAddress tcpAddress;
    private TopicKey topicKey;
    private TopicEncodingKey encoding;

    public ChangeEvent(Type type, ComponentType componentType, String componentName, ComponentId componentId) {
        this(type, componentType, componentId, null);
        this.componentName = componentName;
    }

    public ChangeEvent(Type type, ComponentType componentType, ComponentId componentId, TcpAddress tcpAddress) {
        this.type = type;

        this.componentId = componentId;
        this.containerId = componentId.getContainerId();
        this.tcpAddress = tcpAddress;
        this.componentType = componentType;
    }

    public ChangeEvent(Type type, ComponentType componentType, TopicKey topicKey, TopicEncodingKey encoding) {
        this.type = type;
        this.componentType = componentType;
        this.topicKey = topicKey;
        this.containerId = topicKey.getComponentId().getContainerId();
        this.encoding = encoding;
    }

    public Type getType() {
        return type;
    }

    public String getComponentName() {
        return componentName;
    }

    public ComponentId getComponentId() {
        return componentId;
    }

    public String getContainerId() {
        return containerId;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public TcpAddress getTcpAddress() {
        return tcpAddress;
    }

    public TopicKey getTopicKey() {
        return topicKey;
    }

    public TopicEncodingKey getEncoding() {
        return encoding;
    }

    @Override
    public String toString() {
        return "ChangeEvent{" +
               "type=" + type +
               ", componentName='" + componentName + '\'' +
               ", containerId='" + containerId + '\'' +
               ", componentId=" + componentId +
               ", componentType=" + componentType +
               ", tcpAddress=" + tcpAddress +
               ", topicKey=" + topicKey +
               ", encoding=" + encoding +
               '}';
    }
}
