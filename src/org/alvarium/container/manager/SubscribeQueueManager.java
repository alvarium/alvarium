/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.manager;

import org.alvarium.container.ContainerUtils;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.model.SubscriptionConfig;
import org.alvarium.utils.OpenMap;
import org.alvarium.utils.Pair;

import java.util.List;

import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.logger.Logger.Level.DEBUG;
import static org.alvarium.logger.Logger.log;

public class SubscribeQueueManager implements ManagerChangeListener {
    private final OpenMap<ComponentId, Pair<ComponentId, SubscriptionConfig>> componentSubscriptionMap = new OpenMap<>();
    private final OpenMap<ComponentId, ComponentId> schedulerSubscriptionMap = new OpenMap<>();

    public void postponeComponentSubscription(ComponentId subscriber,SubscriptionConfig config) {
        log(SubscribeQueueManager.class, DEBUG, "Invalid subscription: %s, waiting for resolution!", config.getComponentId());
        componentSubscriptionMap.put(config.getComponentId(), new Pair<>(subscriber, config));
    }

    public void postponeSchedulerSubscription(ComponentId subscriber, ComponentId scheduler) {
        log(SubscribeQueueManager.class, DEBUG, "Invalid scheduler subscription: %s, waiting for resolution!", scheduler);
        schedulerSubscriptionMap.put(scheduler, subscriber);
    }

    private void fireComponentResolvable(ComponentId componentId) {
        List<Pair<ComponentId, SubscriptionConfig>> subscribers = componentSubscriptionMap.remove(componentId);
        subscribers.stream().forEach(subscriber -> ContainerUtils.subscribe(subscriber.getU(), subscriber.getV()));
    }

    private void fireSchedulerResolvable(ComponentId schedulerId) {
        List<ComponentId> subscribers = schedulerSubscriptionMap.remove(schedulerId);
        subscribers.stream().forEach(subscriber -> ContainerUtils.subscribeToScheduler(subscriber, schedulerId));
    }

    @Override
    public void componentChanged(ChangeEvent changeEvent) {
        if (changeEvent.getType() == ChangeEvent.Type.add) {
            if (changeEvent.getComponentType() == ComponentType.component) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireComponentResolvable(changeEvent.getComponentId());
                }
            } else if (changeEvent.getComponentType() == ComponentType.scheduler) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireSchedulerResolvable(changeEvent.getComponentId());
                }
            }
        }
    }

    @Override
    public void hostChanged(ChangeEvent changeEvent) {
        if (changeEvent.getType() == ChangeEvent.Type.add) {
            if (changeEvent.getComponentType() == ComponentType.component) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireComponentResolvable(changeEvent.getComponentId());
                }
            } else if (changeEvent.getComponentType() == ComponentType.scheduler) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireSchedulerResolvable(changeEvent.getComponentId());
                }
            }
        }
    }

    @Override
    public void encodingChanged(ChangeEvent changeEvent) {
        if (changeEvent.getType() == ChangeEvent.Type.add) {
            if (changeEvent.getComponentType() == ComponentType.component) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireComponentResolvable(changeEvent.getComponentId());
                }
            } else if (changeEvent.getComponentType() == ComponentType.scheduler) {
                if (componentManager.isResolvable(changeEvent.getComponentId())) {
                    fireSchedulerResolvable(changeEvent.getComponentId());
                }
            }
        }
    }
}
