/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container.manager;

import org.alvarium.component.service.ServiceClient;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TcpAddress;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.alvarium.container.manager.ComponentManager.componentManager;

public class ServiceManager {
    private final Map<String, ServiceClient> clients = new HashMap<>();
    private final Map<String, Set<ComponentId>> services = new HashMap<>();

    public ServiceManager() {
    }

    public void addService(String serviceName, ComponentId service) {
        boolean serviceAdded;
        synchronized (services) {
            Set<ComponentId> serviceList = services.get(serviceName);
            if (serviceList == null) {
                serviceList = new HashSet<>();
                services.put(serviceName, serviceList);
            }
            serviceAdded = serviceList.add(service);
        }

        if (serviceAdded) {
            serviceAdded(serviceName, service);
        }
    }

    public void removeService(String serviceName, ComponentId service) {
        boolean serviceRemoved = false;
        synchronized (services) {
            Set<ComponentId> serviceList = services.get(serviceName);
            if (serviceList != null) {
                serviceRemoved = serviceList.remove(service);
                if (serviceList.isEmpty()) {
                    services.remove(serviceName);
                }
            }
        }
        if (serviceRemoved) {
            serviceRemoved(serviceName, service);
        }
    }

    public ServiceClient getClient(String serviceName) {
        synchronized (clients) {
            if (!clients.containsKey(serviceName)) {
                createClient(serviceName);
            }
            return clients.get(serviceName);
        }
    }

    private void createClient(String serviceName) {
        synchronized (clients) {
            ServiceClient client = new ServiceClient(serviceName);
            synchronized (services) {
                Set<ComponentId> serviceList = services.get(serviceName);
                if (serviceList != null) {
                    for (ComponentId service : serviceList) {
                        TcpAddress address = componentManager.resolve(service);
                        if (address != null) {
                            client.addConnector(service, address);
                        }
                    }
                }
            }
            clients.put(serviceName, client);
        }
    }

    private void serviceAdded(String serviceName, ComponentId service) {
        synchronized (clients) {
            ServiceClient client = clients.get(serviceName);
            if (client != null) {
                TcpAddress address = componentManager.resolve(service);
                if (address != null) {
                    client.addConnector(service, address);
                }
            }
        }
    }

    private void serviceRemoved(String serviceName, ComponentId service) {
        synchronized (clients) {
            ServiceClient client = clients.get(serviceName);
            if (client != null) {
                client.removeConnector(service);
            }
        }
    }
}
