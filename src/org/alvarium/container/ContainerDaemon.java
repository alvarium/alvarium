/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.alvarium.component.data.DataHelper;
import org.alvarium.container.communication.MeshManager;
import org.alvarium.container.communication.NetworkManager;
import org.alvarium.container.datatypes.TcpAddress;

import static org.alvarium.container.LocalProjectLoader.loadConfigFile;
import static org.alvarium.container.manager.ComponentManager.componentManager;
import static org.alvarium.container.monitor.SystemMonitorManager.systemMonitorManager;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;

public class ContainerDaemon {


    public static void main(String[] args) {
        RunParams params = new RunParams();
        JCommander cmdParser = new JCommander(params);
        cmdParser.setProgramName(ContainerDaemon.class.getSimpleName());
        try {
            cmdParser.parse(args);
        } catch (ParameterException e) {
            failToStart(cmdParser, e.getMessage(), 1);
        }

        if (!NetworkManager.isPortAvailable(params.port)) {
            failToStart(cmdParser, "The provided port is not available, please provide an available one.", 2);
        }

        if (!DataHelper.registerAllGenericDataClasses()) {
            failToStart(cmdParser, "Unable to register all the Data Types", 3);
        }

        TcpAddress remotePeer = null;
        if (params.remotePeer != null) {
            try {
                remotePeer = TcpAddress.parse(params.remotePeer);
                log(ContainerDaemon.class, INFORM, "Using remote peer address: " + remotePeer);
            } catch (IllegalArgumentException e) {
                failToStart(cmdParser, "Unable to parse remote peer address: " + e.getMessage(), 4);
            }
        } else {
            log(ContainerDaemon.class, INFORM, "No remote peer provided!");
        }

        NetworkManager networkManager;
        if (params.componentPortRange != null && params.componentPortRange.size() == 2) {
            log(ContainerDaemon.class, INFORM, "Using the provided port range for component ports: " + params.componentPortRange);
            networkManager = new NetworkManager(params.componentPortRange.get(0), params.componentPortRange.get(1));
        } else {
            log(ContainerDaemon.class, INFORM, "Using random strategy for component port allocation ...");
            networkManager = new NetworkManager();
        }

        log(ContainerDaemon.class, INFORM, "Starting container: " + params.containerId + " on port: " + params.port);
        MeshManager meshManager = new MeshManager(params.containerId, params.port, networkManager);
        meshManager.initialise(remotePeer);

        Runtime.getRuntime().addShutdownHook(new Thread("Shutdown Hook") {
            public void run() {
                log(ContainerDaemon.class, INFORM, "Shutting down platform. Closing all components ...");
                componentManager.closeAllComponents();
                log(ContainerDaemon.class, INFORM, "Shutting down platform. Closing all mesh nodes ...");
                meshManager.close();
                log(ContainerDaemon.class, INFORM, "Shutting down platform. Closing system monitor ...");
                systemMonitorManager.close();
            }
        });

        if (params.projectPath != null) {
            log(ContainerDaemon.class, INFORM, "Loading the provided project: " + params.projectPath);

            if (!loadConfigFile(params, meshManager)) {
                log(ContainerDaemon.class, INFORM, "Could not load the project");
            }
        }
    }

    private static void failToStart(JCommander cmdParser, String message, int errorCode) {
        log(ContainerDaemon.class, CRITICAL, "Error running " + ContainerDaemon.class.getSimpleName() + ": " + message);
        StringBuilder sb = new StringBuilder();
        cmdParser.usage(sb);
        log(ContainerDaemon.class, CRITICAL, sb.toString());
        System.exit(errorCode);
    }
}
