/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.container;

import com.beust.jcommander.Parameter;

import java.util.List;

public class RunParams {
    @Parameter(names = "-container", description = "Container unique ID", required = true)
    public String containerId = null;

    @Parameter(names = "-port", description = "Container port", required = true)
    public int port = -1;

    @Parameter(names = "-portRange", arity = 2, description = "Component port range: <start> <end>")
    public List<Integer> componentPortRange;

    @Parameter(names = "-remotePeer", description = "The address of a remote peer or nothing if the first node. Format: host:port")
    public String remotePeer;

    @Parameter(names = "-projectPath", description = "Project to be loaded by local container. The remote containers will be broadcasted.")
    public String projectPath;

    @Parameter(names = "-noExitOnWarnings", description = "Print, but not exit on warnings", required = false, echoInput = true)
    public boolean noExitOnWarnings = false;

    @Parameter(names = "-ignoreWarnings", description = "Do not display or exit on warnings.", required = false, echoInput = true)
    public boolean ignoreWarnings = false;
}
