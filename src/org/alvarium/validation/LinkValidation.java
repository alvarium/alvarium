/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TopicKey;
import org.alvarium.container.xml.*;

import java.util.*;

public class LinkValidation implements ModelValidation {
    public void checkModel(ProjectXmlConfig model, ModelValidationResult result) {
        Set<TopicKey> externalChannels = new HashSet<>();
        Map<TopicKey, String> externalChannelCounter = new HashMap<>();
        Set<ComponentId> schedulers = new HashSet<>();

        for (ContainerXmlConfig container : model.getContainers()) {
            for (SchedulerXmlConfig schedulerXmlConfig : container.getSchedulerConfigs()) {
                schedulers.add(new ComponentId(container.getContainerId(), schedulerXmlConfig.getId()));
            }

            for (ComponentXmlConfig component : container.getComponentConfigs()) {
                try {
                    ComponentId componentId = new ComponentId(container.getContainerId(), component.getId());
                    Class clazz = Class.forName(component.getComponentClass());
                    Set<String> internalChannels = new HashSet<>();

                    if (clazz.isAnnotationPresent(ConfigureParams.class)) {
                        ConfigureParams annotation = (ConfigureParams) clazz.getAnnotation(ConfigureParams.class);
                        internalChannels.addAll(Arrays.asList(annotation.outputChannels()));
                    }

                    for (PublishXmlConfig publishChannel : component.getPublishConfigs()) {
                        if (!internalChannels.contains(publishChannel.getInternalTopic())) {
                            result.addErrorMessage("Invalid internal channel binding: " + publishChannel
                                                   + " * Component: " + componentId);
                        }
                        externalChannels.add(new TopicKey(publishChannel.getTopic(), componentId));
                        externalChannelCounter.put(new TopicKey(publishChannel
                                                                        .getTopic(), componentId), publishChannel + "" +
                                                                                                   " * Component: " +
                                                                                                   componentId);
                    }
                } catch (ClassNotFoundException e) {
                    //-- ignore
                }
            }
        }

        for (ContainerXmlConfig container : model.getContainers()) {
            for (ComponentXmlConfig component : container.getComponentConfigs()) {
                for (SubscriptionXmlConfig subscribeChannel : component.getSubscriptionConfigs()) {
                    ComponentId componentId = new ComponentId(subscribeChannel.getContainerId(), subscribeChannel
                                                                                                         .getComponentId());
                    TopicKey topicKey = new TopicKey(subscribeChannel.getTopic(), componentId);
                    if (externalChannels.contains(topicKey)) {
                        externalChannelCounter.remove(topicKey);
                    } else {
                        result.addErrorMessage("Invalid subscribe channel binding. No publish link for: " +
                                               subscribeChannel + " * Component: " + component.getId());
                    }
                }
                SchedulerPropertyXmlConfig propertyXmlConfig = component.getScheduler();
                if (propertyXmlConfig != null) {
                    ComponentId schedulerId = new ComponentId(propertyXmlConfig.getContainer(), propertyXmlConfig.getId());
                    if (!schedulers.contains(schedulerId)) {
                        result.addErrorMessage("Invalid invalid subscription to scheduler: " + schedulerId +
                                               " * Component: " + component.getId());
                    }
                }
            }
        }

        for (String externalChannel : externalChannelCounter.values()) {
            result.addWarningMessage("Invalid publish channel binding. No subscription link for: " + externalChannel);
        }
    }
}
