/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.SchedulerConfig;
import org.alvarium.container.model.ServiceConfig;
import org.alvarium.container.xml.*;
import org.alvarium.component.service.Scheduler;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ComponentsValidation implements ModelValidation {
    public void checkModel(ProjectXmlConfig model, ModelValidationResult result) {
        Set<ComponentId> componentIds = new HashSet<>();
        for (ContainerXmlConfig container : model.getContainers()) {
            validateComponents(container.getContainerId(), container.getComponentConfigs(), componentIds, result);
            validateSchedulers(container.getContainerId(), container.getSchedulerConfigs(), componentIds, result);
            validateServices(container.getContainerId(), container.getServiceConfigs(), componentIds, result);
        }
    }

    private void validateComponents(String containerId, List<ComponentXmlConfig> xmlConfigs,
                                    Set<ComponentId> componentIds, ModelValidationResult result) {
        Class[] classTypes = new Class[]{ComponentConfig.class};
        for (ComponentXmlConfig component : xmlConfigs) {
            ComponentId componentId = new ComponentId(containerId, component.getId());
            validate(componentId, component.getComponentClass(), classTypes, componentIds, result);
        }
    }

    private void validateSchedulers(String containerId, List<SchedulerXmlConfig> xmlConfigs,
                                    Set<ComponentId> componentIds, ModelValidationResult result) {
        Class[] classTypes = new Class[]{SchedulerConfig.class};
        String schedulerClassName = Scheduler.class.getCanonicalName();
        for (SchedulerXmlConfig component : xmlConfigs) {
            ComponentId componentId = new ComponentId(containerId, component.getId());
            validate(componentId, schedulerClassName, classTypes, componentIds, result);
        }
    }

    private void validateServices(String containerId, List<ServiceXmlConfig> xmlConfigs,
                                  Set<ComponentId> componentIds, ModelValidationResult result) {
        Class[] classTypes = new Class[]{ServiceConfig.class};
        for (ServiceXmlConfig component : xmlConfigs) {
            ComponentId componentId = new ComponentId(containerId, component.getId());
            validate(componentId, component.getServiceClass(), classTypes, componentIds, result);
        }
    }

    private void validate(ComponentId componentId, String className, Class[] constructorParameters,
                          Set<ComponentId> componentIds, ModelValidationResult result) {
        if (componentIds.contains(componentId)) {
            result.addErrorMessage("Duplicate component Id: " + componentId);
        } else {
            componentIds.add(componentId);
            try {
                Class<?> clazz = Class.forName(className);
                Constructor constructor = clazz.getConstructor(constructorParameters);
                if (constructor != null) {
                    constructor.setAccessible(true);
                }
            } catch (NoSuchMethodException e) {
                result.addErrorMessage("Invalid class constructor for " + className);
            } catch (ClassNotFoundException e) {
                result.addErrorMessage("Invalid class provided: " + className);
            }
        }
    }
}
