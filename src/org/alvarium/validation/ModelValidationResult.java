/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class ModelValidationResult {
    private Set<String> errorList = null;
    private Set<String> warningList = null;

    public ModelValidationResult() {
    }

    public void addErrorMessage(String errorMessage) {
        if (errorList == null) {
            errorList = new LinkedHashSet<>();
        }
        errorList.add(errorMessage);
    }

    public void addErrorMessages(Set<String> errorMessages) {
        if (errorList == null) {
            errorList = new LinkedHashSet<>();
        }
        errorList.addAll(errorMessages);
    }

    public Set<String> getErrorList() {
        if (errorList == null) {
            return Collections.emptySet();
        } else {
            return errorList;
        }
    }

    public boolean hasErrors() {
        return errorList != null && !errorList.isEmpty();
    }

    public void addWarningMessage(String warningMessage) {
        if (warningList == null) {
            warningList = new LinkedHashSet<>();
        }
        warningList.add(warningMessage);
    }

    public void addWarningMessages(Set<String> warningMessages) {
        if (warningList == null) {
            warningList = new LinkedHashSet<>();
        }
        warningList.addAll(warningMessages);
    }

    public Set<String> getWarningList() {
        if (warningList == null) {
            return Collections.emptySet();
        } else {
            return warningList;
        }
    }

    public boolean hasWarnings() {
        return warningList != null && !warningList.isEmpty();
    }

    public boolean isValid() {
        return !hasErrors() && !hasWarnings();
    }
}
