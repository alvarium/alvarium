/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import org.alvarium.container.xml.*;
import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.datatypes.TopicKey;

import java.util.*;
import java.util.stream.Collectors;

public class ComponentDataTypeValidation implements ModelValidation {
    public void checkModel(ProjectXmlConfig model, ModelValidationResult result) {
        Map<TopicKey, Class> publishData = new HashMap<>();
        Map<TopicKey, List<Class<?>>> subscriptionData = new HashMap<>();

        for (ContainerXmlConfig container : model.getContainers()) {
            for (ComponentXmlConfig component : container.getComponentConfigs()) {
                ComponentId componentId = new ComponentId(container.getContainerId(), component.getId());
                try {
                    Class clazz = Class.forName(component.getComponentClass());
                    if (clazz.isAnnotationPresent(ConfigureParams.class)) {
                        ConfigureParams annotation = (ConfigureParams) clazz.getAnnotation(ConfigureParams.class);
                        boolean valid = validateOutputChannels(component, annotation, result);
                        if (valid) {
                            validatePublishChannels(componentId, component, annotation, publishData, result);
                            generateInputChannels(component, annotation, subscriptionData);
                        }
                    }
                } catch (ClassNotFoundException e) {
                    //-- ignore
                }
            }
        }

        crossValidate(subscriptionData, publishData, result);
    }

    private boolean validateOutputChannels(ComponentXmlConfig component, ConfigureParams annotation,
                                           ModelValidationResult result) {
        if (annotation.outputChannels().length > 0 && annotation.outputChannels()[0].trim().length() > 0) {
            if (annotation.outputChannels().length != annotation.outputDataTypes().length) {
                result.addErrorMessage("Class: " + component.getComponentClass() +
                        " does not properly annotate the outputDataTypes");
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private void validatePublishChannels(ComponentId componentId, ComponentXmlConfig component,
                                         ConfigureParams annotation, Map<TopicKey, Class> publishData,
                                         ModelValidationResult result) {
        for (PublishXmlConfig publishChannel : component.getPublishConfigs()) {
            boolean dataTypeDetermined = false;
            for (int i = 0; !dataTypeDetermined && i < annotation.outputChannels().length; i++) {
                if (annotation.outputChannels()[i].equals(publishChannel.getInternalTopic())) {
                    TopicKey key = new TopicKey(publishChannel.getTopic(), componentId);
                    publishData.put(key, annotation.outputDataTypes()[i]);
                    dataTypeDetermined = true;
                }
            }
            if (!dataTypeDetermined) {
                result.addErrorMessage("Could not determine the DataType of the channel: " + publishChannel +
                        " * This could be linked to an invalid internal channel linking.");
            }
        }
    }

    private void generateInputChannels(ComponentXmlConfig component, ConfigureParams annotation,
                                       Map<TopicKey, List<Class<?>>> subscribeData) {
        for (SubscriptionXmlConfig subscribeChannel : component.getSubscriptionConfigs()) {
            ComponentId componentId = new ComponentId(subscribeChannel.getContainerId(), subscribeChannel.getComponentId());
            TopicKey key = new TopicKey(subscribeChannel.getTopic(), componentId);
            List<Class<?>> inputDataTypes = Arrays.asList(annotation.inputDataTypes()).stream()
                    .filter(clazz -> clazz != org.alvarium.component.data.Void.class).collect(Collectors.toList());
            inputDataTypes.remove(Void.class);
            subscribeData.put(key, inputDataTypes);
        }
    }

    private void crossValidate(Map<TopicKey, List<Class<?>>> subscribeData, Map<TopicKey, Class> publishData,
                               ModelValidationResult result) {
        for (Map.Entry<TopicKey, Class> publish : publishData.entrySet()) {
            if (publish.getValue() != null) {
                boolean foundCompatibleInput = false;
                Class channelDataType = publish.getValue();
                List<Class<?>> inputDataTypes = subscribeData.get(publish.getKey());
                if (inputDataTypes != null) {
                    Iterator<Class<?>> iterator = inputDataTypes.iterator();
                    while (!foundCompatibleInput && iterator.hasNext()) {
                        Class<?> inputDataType = iterator.next();
                        if (inputDataType.isAssignableFrom(channelDataType)) {
                            foundCompatibleInput = true;
                        }
                    }
                    if (!foundCompatibleInput) {
                        result.addErrorMessage("Component " + publish.getKey().getComponentId() + " subscribe topic " +
                                publish.getKey().getTopic() + " does not match the accepted input. " +
                                channelDataType + " is not compatible with " + inputDataTypes);
                    }
                }
            } else {
                result.addErrorMessage("Could not determine the DataType of the channel: " + publish.getKey());
            }
        }
    }
}
