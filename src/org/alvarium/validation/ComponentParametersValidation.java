/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.container.datatypes.ComponentId;
import org.alvarium.container.xml.ComponentXmlConfig;
import org.alvarium.container.xml.ContainerXmlConfig;
import org.alvarium.container.xml.ProjectXmlConfig;
import org.alvarium.container.xml.PropertyXmlConfig;

import java.util.List;

public class ComponentParametersValidation implements ModelValidation {
    private enum ParamType {
        mandatory("Mandatory", true),
        optional("Optional", false);

        private String name;
        private boolean isError;

        ParamType(String name, boolean isError) {
            this.name = name;
            this.isError = isError;
        }
    }

    public void checkModel(ProjectXmlConfig model, ModelValidationResult result) {
        for (ContainerXmlConfig container : model.getContainers()) {
            for (ComponentXmlConfig component : container.getComponentConfigs()) {
                try {
                    Class clazz = Class.forName(component.getComponentClass());
                    if (clazz.isAnnotationPresent(ConfigureParams.class)) {
                        ConfigureParams annotation = (ConfigureParams) clazz.getAnnotation(ConfigureParams.class);
                        ComponentId componentId = new ComponentId(container.getContainerId(), component.getId());
                        validateParams(ParamType.mandatory, annotation.mandatoryConfigurationParams(),
                                component.getPropertyConfigs(), componentId, result);
                        validateParams(ParamType.optional, annotation.optionalConfigurationParams(),
                                component.getPropertyConfigs(), componentId, result);
                    }
                } catch (ClassNotFoundException e) {
                    //-- ignore
                }
            }
        }
    }

    private void validateParams(ParamType paramType, String[] params, List<PropertyXmlConfig> properties,
                                ComponentId componentId, ModelValidationResult result) {
        for (String param : params) {
            param = param.trim();
            if (param.length() > 0) {
                boolean hasProperty = false;
                for (PropertyXmlConfig property : properties) {
                    if (property.getProperty().equals(param)) {
                        hasProperty = true;
                    }
                }
                if (!hasProperty) {
                    String msg = paramType.name + " configuration parameter: " + param + " is missing for component: " +
                            componentId;
                    if (paramType.isError) {
                        result.addErrorMessage(msg);
                    } else {
                        result.addWarningMessage(msg);
                    }
                }
            }
        }
    }
}
