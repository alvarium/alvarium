/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.validation;

import org.alvarium.container.xml.ProjectManager;
import org.alvarium.container.xml.ProjectXmlConfig;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class ModelValidationManager {
    private final List<ModelValidation> validators = new LinkedList<ModelValidation>();

    public void addValidator(ModelValidation validator) {
        synchronized (validators) {
            validators.add(validator);
        }
    }

    public void removeValidator(ModelValidation validator) {
        synchronized (validators) {
            validators.remove(validator);
        }
    }

    public ModelValidationResult checkComponentModel(ProjectXmlConfig model, boolean haltOnFirstError) {
        ModelValidationResult result = new ModelValidationResult();
        synchronized (validators) {
            for (ModelValidation modelValidation : validators) {
                modelValidation.checkModel(model, result);
                if (result.hasErrors() && haltOnFirstError) {
                    return result;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        ModelValidationManager manager = new ModelValidationManager();
        manager.addValidator(new ComponentsValidation());
        manager.addValidator(new LinkValidation());
        manager.addValidator(new ComponentParametersValidation());
        manager.addValidator(new ComponentDataTypeValidation());

        ProjectXmlConfig model = ProjectManager.load(new File("container1.xml"));
        ModelValidationResult result = manager.checkComponentModel(model, false);
        if (result.isValid()) {
            System.out.println("Model is valid ...");
        } else {
            if (result.hasErrors()) {
                System.out.println("Model Errors:");
                for (String item : result.getErrorList()) {
                    System.out.println("\t" + item);
                }
            }

            if (result.hasWarnings()) {
                System.out.println("Model Warnings:");
                for (String item : result.getWarningList()) {
                    System.out.println("\t" + item);
                }
            }
        }
    }
}
