/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.bricks;


import org.alvarium.component.annotations.ConfigureParams;
import org.alvarium.component.base.SourceComponent;
import org.alvarium.component.data.StringData;
import org.alvarium.container.model.ComponentConfig;
import org.alvarium.container.model.ListProperties;
import org.alvarium.logger.Logger;
import org.alvarium.utils.LanguageUtils;

@ConfigureParams(outputChannels = {"test1", "test2"}, outputDataTypes = {StringData.class, StringData.class})
public class Test1 extends SourceComponent {
    private int i;

    public Test1(ComponentConfig config) {
        super(config);
    }

    protected void setupComponent(ListProperties config) {
        i = 0;
    }

    public boolean act() {
        publishData("test1", new StringData("" + i, "session1", "Hello-t1:" + i, LanguageUtils.IDX_NONE));
        Logger.log(this, Logger.Level.DEBUG, "Test1 act");
        i++;
//        publishData("test2", new StringData("Hello-t2"));
        return true;
    }
}
