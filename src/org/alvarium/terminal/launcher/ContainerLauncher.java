/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.terminal.launcher;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.alvarium.component.data.DataHelper;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.exception.ComponentStartupException;
import org.alvarium.container.model.ProjectConfig;
import org.alvarium.container.xml.ProjectManager;
import org.alvarium.container.xml.ProjectXmlConfig;
import org.alvarium.protocol.parameters.ProjectRelatedParameter;
import org.alvarium.protocol.processor.FailResult;
import org.alvarium.protocol.processor.ProcessingResult;
import org.alvarium.protocol.processor.RequestProcessor;
import org.alvarium.terminal.TerminalConnector;
import org.alvarium.validation.*;

import java.io.File;

import static org.alvarium.container.model.GenericConverter.genericConverter;
import static org.alvarium.logger.Logger.Level;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.ProtocolRole.admin;
import static org.alvarium.protocol.command.AdminProtocolCommands.*;
import static org.alvarium.protocol.command.GenericProtocolCommands.*;

public class ContainerLauncher {
    private static final ModelValidationManager modelValidationManager = new ModelValidationManager();

    static {
        modelValidationManager.addValidator(new ComponentsValidation());
        modelValidationManager.addValidator(new LinkValidation());
        modelValidationManager.addValidator(new ComponentParametersValidation());
        modelValidationManager.addValidator(new ComponentDataTypeValidation());
    }

    private static RunParams runParams = new RunParams();

    private static TerminalConnector terminalConnector;
    private static RequestProcessor processor;

    private static File currentRoot = new File(".");

    public static void main(String[] args) {
        JCommander cmdParser = new JCommander(runParams);
        cmdParser.setProgramName(ContainerLauncher.class.getSimpleName());

        try {
            cmdParser.parse(args);
        } catch (ParameterException e) {
            failToStart(cmdParser, e.getMessage(), 1);
        }

        if (!DataHelper.registerAllGenericDataClasses()) {
            failToStart("Unable to register all the Data Types", 2);
        }

        TcpAddress remotePeer = null;
        if (runParams.remotePeer != null) {
            try {
                remotePeer = TcpAddress.parse(runParams.remotePeer);
                log(ContainerLauncher.class, INFORM, "Using remote peer address: " + remotePeer);
            } catch (IllegalArgumentException e) {
                failToStart(cmdParser, "Unable to parse remote peer address: " + e.getMessage(), 3);
            }
        } else {
            failToStart(cmdParser, "No remote peer provided!", 4);
        }

        terminalConnector = new TerminalConnector(remotePeer);
        terminalConnector.open();
        processor = new RequestProcessor(terminalConnector.getAddress().toPattern(), admin, terminalConnector)
                .addBusyReply(buzzy).setMaxRepeatsOnBusy(10);

        if (!initialiseConnector()) {
            failToStart("Could not initialise terminal connection to mesh node!", 5);
        }

        if (runParams.configPathname != null) {
            if (loadConfigFile(runParams.configPathname)) {
                System.exit(0);
            } else {
                System.exit(1);
            }
        } else if (runParams.statusCommand) {
            status();
            System.exit(0);
        } else {
            failToStart("At least one option config or status should be provided", 9);
        }
    }

    private static boolean initialiseConnector() {
        log(ContainerLauncher.class, INFORM, "Saluting the peer node ...");
        ProcessingResult result = processor.syncRequest(buzz, ANY).addValidReply(buzzEcho).process(true);
        if (result instanceof FailResult) {
            FailResult.FailReason failReason = ((FailResult) result).getFailReason();
            log(ContainerLauncher.class, INFORM, "Failed to connect to remote peer! Reason:  " + failReason);
        } else {
            terminalConnector.setConnected(true);
            log(ContainerLauncher.class, INFORM, "Salut complete!");
        }
        return terminalConnector.isConnected();
    }

    private static void status() {
        String message = "Peer connection: " + terminalConnector.getAddress()
                + " connected=" + terminalConnector.isConnected();
        log(ContainerLauncher.class, Level.INFORM, message);
    }

    private static boolean loadConfigFile(String configPathname) {
        File configFilename = new File(currentRoot, configPathname);
        if (!configFilename.canRead()) {
            log(ContainerLauncher.class, Level.CRITICAL, "File not found: " + configFilename.getAbsolutePath());
            return false;
        }
        ProjectXmlConfig projectXmlConfig = ProjectManager.load(configFilename);
        ModelValidationResult validationResult = modelValidationManager.checkComponentModel(projectXmlConfig, false);
        if (validationResult.hasErrors()) {
            for (String line : validationResult.getErrorList()) {
                log(ContainerLauncher.class, Level.INFORM, "Error: " + line);
            }
            log(ContainerLauncher.class, Level.CRITICAL, "Too manny errors while executing load. Aborted!");
            return false;
        }

        if (!runParams.ignoreWarnings) {
            if (validationResult.hasWarnings()) {
                for (String line : validationResult.getWarningList()) {
                    log(ContainerLauncher.class, Level.INFORM, "Warning: " + line);
                }
                if (!runParams.noExitOnWarnings) {
                    log(ContainerLauncher.class, Level.CRITICAL, "Too manny warnings while executing load. Aborted!");
                    return false;
                }
            }
        }

        ProjectConfig config;
        try {
            config = genericConverter.convert(projectXmlConfig, false);
        } catch (ComponentStartupException e) {
            e.printStackTrace();
            log(ContainerLauncher.class, Level.CRITICAL, "Got exception: " + e.getCause().getMessage());
            log(ContainerLauncher.class, Level.CRITICAL, "ComponentID: " + e.getComponentId());
            log(ContainerLauncher.class, Level.CRITICAL, "Cannot start platform ... exiting the current instance !");
            return false;
        }
        if (config != null) {
            log(ContainerLauncher.class, Level.INFORM, "Sending load message, waiting for reply");
            ProcessingResult result = processor.syncRequest(adminLoadProject, ANY, new ProjectRelatedParameter(config))
                                               .addValidReply(adminLoadProjectAck).addValidReply(adminLoadProjectRej)
                                               .setMaxRepeatsOnBusy(10).process();
            log(ContainerLauncher.class, Level.INFORM, "Reply is valid: " + result.isValid());
            log(ContainerLauncher.class, Level.INFORM, "Reply received: " + result.getReplyMessage());
            return result.isValid() && result.getReplyMessage().getProtocolCommand() == adminLoadProjectAck;
        } else {
            return false;
        }
    }

    private static void failToStart(String message, int errorCode) {
        failToStart(null, message, errorCode);
    }

    private static void failToStart(JCommander cmdParser, String message, int errorCode) {
        log(ContainerLauncher.class, CRITICAL,
            "Error running " + ContainerLauncher.class.getSimpleName() + ": " + message);
        if (cmdParser != null) {
            StringBuilder sb = new StringBuilder();
            cmdParser.usage(sb);
            log(ContainerLauncher.class, Level.CRITICAL, sb.toString());
        }
        System.exit(errorCode);
    }
}
