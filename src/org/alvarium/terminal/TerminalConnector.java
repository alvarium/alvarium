/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.terminal;

import org.alvarium.component.data.InvalidDataException;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.protocol.ProtocolMessage;
import org.alvarium.protocol.RemoteConnector;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

import java.util.Collections;
import java.util.Set;

import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.MessageFactory.messageFactory;
import static org.alvarium.protocol.ProtocolMessage.invalidMessage;

public class TerminalConnector implements RemoteConnector {
    private TcpAddress address;

    private ZMQ.Context context;
    private ZMQ.Socket sender;

    private boolean connected = false;
    private long lastPing = 0;

    public TerminalConnector(TcpAddress address) {
        this.address = address;

        context = ZMQ.context(1);
        sender = context.socket(ZMQ.REQ);

        // todo; a more generic solution would be preferred
        sender.setReceiveTimeOut(10000);
        sender.setLinger(10000);
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public ProtocolMessage request(ProtocolMessage message) {
        try {
            byte[] request = messageFactory.writeObject(message);
            if (!sender.send(request)) {
                log(TerminalConnector.class, CRITICAL, "Failed to send message: " + message);
                return invalidMessage;
            } else {
                lastPing = System.currentTimeMillis();
                return messageFactory.readObject(sender.recv(0));
            }
        } catch (InvalidDataException | ZMQException e) {
            log(TerminalConnector.class, CRITICAL, "Invalid data on terminal connector!", e);
        }
        return invalidMessage;
    }

    @Override
    public Set<String> getRemoteContainerIds() {
        return Collections.singleton("*");
    }

    @Override
    public long getLastPing(String remoteId) {
        return lastPing;
    }

    public TcpAddress getAddress() {
        return address;
    }

    public void open() {
        String connectPattern = String.format("%s://%s:%s", TcpAddress.protocol, address.getHost(), address.getPort());
        sender.connect(connectPattern);
    }

    public void close() {
        sender.close();
        context.term();
    }
}
