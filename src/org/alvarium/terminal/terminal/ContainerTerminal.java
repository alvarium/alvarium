/*
 *  Copyright (c) Alvarium Project Maintainers
 *  Original author of the Alvarium Project is Ovidiu Serban, ovidiu@roboslang.org
 *  All Rights Reserved. Use is subject to license terms.
 *
 *  The usage of this project makes mandatory the authors citation in
 *  any scientific publication or technical reports. For websites or
 *  research projects the Alvarium website and logo needs to be linked
 *  in a visible area. Please check the project website for more details.
 *
 *  All the files of the Alvarium Project are subject of this license,
 *   unless stated otherwise. All the libraries, sounds and graphic elements
 *   used in the project are subject to their own license.
 *
 *   Alvarium is free software: you can redistribute them and/or modify
 *   them under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, version 3 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.alvarium.terminal.terminal;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.alvarium.component.data.DataHelper;
import org.alvarium.container.datatypes.TcpAddress;
import org.alvarium.container.exception.ComponentStartupException;
import org.alvarium.container.model.ProjectConfig;
import org.alvarium.container.xml.ProjectManager;
import org.alvarium.container.xml.ProjectXmlConfig;
import org.alvarium.protocol.parameters.ProjectRelatedParameter;
import org.alvarium.protocol.processor.FailResult;
import org.alvarium.protocol.processor.ProcessingResult;
import org.alvarium.protocol.processor.RequestProcessor;
import org.alvarium.terminal.TerminalConnector;
import org.alvarium.validation.*;
import simple.terminal.BasicTerminal;
import simple.terminal.TerminalLogger;
import simple.terminal.command.CommandManager;
import simple.terminal.command.TerminalCommand;
import simple.terminal.validation.ValidationException;

import java.io.File;
import java.io.IOException;

import static org.alvarium.container.model.GenericConverter.genericConverter;
import static org.alvarium.logger.Logger.Level;
import static org.alvarium.logger.Logger.Level.CRITICAL;
import static org.alvarium.logger.Logger.Level.INFORM;
import static org.alvarium.logger.Logger.log;
import static org.alvarium.protocol.ProtocolMessage.ANY;
import static org.alvarium.protocol.ProtocolRole.admin;
import static org.alvarium.protocol.command.AdminProtocolCommands.*;
import static org.alvarium.protocol.command.GenericProtocolCommands.*;

public class ContainerTerminal {
    private static final ModelValidationManager modelValidationManager = new ModelValidationManager();

    static {
        modelValidationManager.addValidator(new ComponentsValidation());
        modelValidationManager.addValidator(new LinkValidation());
        modelValidationManager.addValidator(new ComponentParametersValidation());
        modelValidationManager.addValidator(new ComponentDataTypeValidation());
    }

    public static final String CMD_PWD = "pwd";
    public static final String CMD_LS = "ls";
    public static final String CMD_CD = "cd";
    public static final String CMD_EXIT = "exit";

    public static final String CMD_STATUS = "status";
    public static final String CMD_LOAD = "load";

    private static RunParams runParams = new RunParams();

    private static TerminalConnector terminalConnector;
    private static RequestProcessor processor;

    private static File currentRoot = new File(".");

    public static void main(String[] args) {
        JCommander cmdParser = new JCommander(runParams);
        cmdParser.setProgramName(ContainerTerminal.class.getSimpleName());

        try {
            cmdParser.parse(args);
        } catch (ParameterException e) {
            failToStart(cmdParser, e.getMessage(), 1);
        }

        if (!DataHelper.registerAllGenericDataClasses()) {
            failToStart("Unable to register all the Data Types", 2);
        }

        TcpAddress remotePeer = null;
        if (runParams.remotePeer != null) {
            try {
                remotePeer = TcpAddress.parse(runParams.remotePeer);
                log(ContainerTerminal.class, INFORM, "Using remote peer address: " + remotePeer);
            } catch (IllegalArgumentException e) {
                failToStart(cmdParser, "Unable to parse remote peer address: " + e.getMessage(), 3);
            }
        } else {
            failToStart(cmdParser, "No remote peer provided!", 4);
        }

        terminalConnector = new TerminalConnector(remotePeer);
        terminalConnector.open();
        processor = new RequestProcessor(terminalConnector.getAddress().toPattern(), admin, terminalConnector)
                            .addBusyReply(buzzy).setMaxRepeatsOnBusy(10);

        if (!initialiseConnector()) {
            failToStart("Could not initialise terminal connection to mesh node!", 5);
        }

        try {
            CommandManager commandManager = new CommandManager();
            BasicTerminal terminal = new BasicTerminal(commandManager, commandManager);

            commandManager.addCommand(new TerminalCommand(CMD_LOAD, ContainerTerminal::loadConfigFile, 1));
            commandManager.addCommand(new TerminalCommand(CMD_CD, ContainerTerminal::changeFolder, 1));
            commandManager.addCommand(new TerminalCommand(CMD_LS, ContainerTerminal::listFolder, 0));
            commandManager.addCommand(new TerminalCommand(CMD_PWD, ContainerTerminal::pwd, 0));
            commandManager.addCommand(new TerminalCommand(CMD_EXIT, terminal::exitTerminal, 0));
            commandManager.addCommand(new TerminalCommand(CMD_STATUS, ContainerTerminal::status, 0));

            terminal.addListener(terminalConnector::close);
            terminal.process();
        } catch (IOException e) {
            failToStart("Terminal exception: " + e, 9);
        }
    }

    private static boolean initialiseConnector() {
        log(ContainerTerminal.class, INFORM, "Saluting the peer node ...");
        ProcessingResult result = processor.syncRequest(buzz, ANY).addValidReply(buzzEcho).process(true);
        if (result instanceof FailResult) {
            FailResult.FailReason failReason = ((FailResult) result).getFailReason();
            log(ContainerTerminal.class, INFORM, "Failed to connect to remote peer! Reason:  " + failReason);
        } else {
            terminalConnector.setConnected(true);
            log(ContainerTerminal.class, INFORM, "Salut complete!");
        }
        return terminalConnector.isConnected();
    }

    private static boolean listFolder(TerminalCommand.Params params) {
        TerminalLogger terminalLogger = params.getLogger();
        File[] list = currentRoot.listFiles();
        if (list != null) {
            for (File file : list) {
                terminalLogger.log(CMD_LS, file.getName());
            }
        }
        return true;
    }

    private static boolean pwd(TerminalCommand.Params params) {
        TerminalLogger terminalLogger = params.getLogger();
        terminalLogger.log(CMD_PWD, currentRoot.getAbsolutePath());
        return true;
    }

    private static boolean changeFolder(TerminalCommand.Params params) {
        File newFolder = new File(currentRoot, params.getValues()[0]);
        if (newFolder.isDirectory() && newFolder.canRead()) {
            currentRoot = newFolder;
            return true;
        } else {
            return false;
        }
    }

    private static boolean status(TerminalCommand.Params params) {
        TerminalLogger terminalLogger = params.getLogger();
        String message = "Peer connection: " + terminalConnector.getAddress()
                         + " connected=" + terminalConnector.isConnected();
        terminalLogger.log(CMD_STATUS, message);
        return true;
    }

    private static boolean loadConfigFile(TerminalCommand.Params params) {
        TerminalLogger terminalLogger = params.getLogger();

        File configFilename = new File(currentRoot, params.getValues()[0]);
        if (!configFilename.canRead()) {
            throw new ValidationException("File not found: " + configFilename.getAbsolutePath());
        }
        ProjectXmlConfig projectXmlConfig = ProjectManager.load(configFilename);
        ModelValidationResult validationResult = modelValidationManager.checkComponentModel(projectXmlConfig, false);
        if (validationResult.hasErrors()) {
            for (String line : validationResult.getErrorList()) {
                terminalLogger.log(CMD_LOAD, "Error: " + line);
            }
            throw new ValidationException("Too manny errors while executing load. Aborted!");
        }

        if (!runParams.ignoreWarnings) {
            if (validationResult.hasWarnings()) {
                for (String line : validationResult.getWarningList()) {
                    terminalLogger.log(CMD_LOAD, "Warning: " + line);
                }
                if (!runParams.noExitOnWarnings) {
                    throw new ValidationException("Too manny warnings while executing load. Aborted!");
                }
            }
        }

        ProjectConfig config;
        try {
            config = genericConverter.convert(projectXmlConfig, false);
        } catch (ComponentStartupException e) {
            e.printStackTrace();
            terminalLogger.log(CMD_LOAD, "Got exception: " + e.getCause().getMessage());
            terminalLogger.log(CMD_LOAD, "ComponentID: " + e.getComponentId());
            terminalLogger.log(CMD_LOAD, "Cannot start platform ... exiting the current instance !");
            return false;
        }
        if (config != null) {
            terminalLogger.log(CMD_LOAD, "Sending load message, waiting for reply");
            processor.asyncRequest(adminLoadProject, ANY, new ProjectRelatedParameter(config))
                     .onFail(failResult ->
                                     terminalLogger.log(CMD_LOAD, "Invalid. Reason: " + failResult.getFailReason() +
                                                                  " Remote reason: " + failResult.getReplyMessage()))
                     .onSuccess(adminLoadProjectAck, result -> terminalLogger.log(CMD_LOAD, "Ack received, remote " +
                                                                                            "peer loaded the project " +
                                                                                            "properly"))
                     .onSuccess(adminLoadProjectRej, result -> terminalLogger.log(CMD_LOAD, "Reject received from " +
                                                                                            "remote peer: " + result.getReplyMessage().getProtocolParameter()))
                     .process();

            return true;
        } else {
            return false;
        }
    }

    private static void failToStart(String message, int errorCode) {
        failToStart(null, message, errorCode);
    }

    private static void failToStart(JCommander cmdParser, String message, int errorCode) {
        log(ContainerTerminal.class, CRITICAL,
            "Error running " + ContainerTerminal.class.getSimpleName() + ": " + message);
        if (cmdParser != null) {
            StringBuilder sb = new StringBuilder();
            cmdParser.usage(sb);
            log(ContainerTerminal.class, Level.CRITICAL, sb.toString());
        }
        System.exit(errorCode);
    }
}
